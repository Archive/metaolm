Module/Client Communication
===========================

.. warning:: This is an early draft document.
	  Everything here will be subject to change.

.. contents::


Definitions
-----------

:Client: The Matrix client utilising metaolm
:Module: The metaolm module

Introduction
------------

As metaolm is a monolithic module for end-to-end encryption in Matrix,
it will need to communicate with the client utilising it, in order to be
useful.
This document will give you an overview on how that communication is
structured and how to implement it.

Basic communication structure
-----------------------------

In order to communicate with each other, a bi-directional communication
channel between client and module is to be established before any other
action.
The channel is implemented using two of Rust's own ``std::sync::mpsc::channel``.
They operate on the first-in-first-out fairness principle.
For more information have a look at metaolms Rustdoc.

To avoid additional heavy dependencies and configuration, metaolm doesn't
provide a way of its own to communicate with the network or store data.
When either functionality is required, the module will ask the
client to do both things for it.

Sometimes an action will be blocked by another, like uploading device
keys: They have to be stored before they can be uploaded.
To coordinate work correctly, the communication between module and client
will contain blocker IDs for specific blocked actions.

.. image:: client_module.png

Communication
-------------

Communication is packet based. In the context of Rust this means,
that over the communication channels specific packet objects
will be sent.

These specific packet objects are the structs ``M2CPacket`` and
``C2MPacket``. C2M here means that the packet object will be sent
from the client to the module and in the case of M2C the same for
module to client.

Packets can be of different types and can be differentiated by
looking at the ``header`` attribute.
Types are implemented as enums.

For actions that block other actions, packets will have the
``blocker_id`` attribute set.
When sending a response to the communication partner, that contained
a ``blocker_id``, the same ID is to be attatched to the response.

To make it clear where the action is blocked, and avoid client and
module using the same ID for different actions,
the module will use even and the client odd numbers to communicate
their own blocked actions.
As a result IDs can simply be incremeted by two, every time another
ID is required.

M2C Packets
-----------

M2C packets have a ``header``, ``details``, ``body`` and
``blocker_id`` attributes.

:header: main differentiator for the functionality of a packet
:details: specific details for a packet, that is better left separated from
	  the body
:body: contains main information of the packet
:blocker_id: optionally defined ID of the event this event is blocking

The following packet types exist and are to be understood as follows:

HTTPReq
~~~~~~~

This is a HTTP request, that is to be forwarded by the client.

:details: HTTP request type, followed by the URL for the request,
	  written in the same style as URLs in the Matrix specification.
	  They are seperated with a space, for example for uploading device
	  keys this would be ``POST /_matrix/client/r0/keys/upload``.
:body: JSON data for the request
:blocker_id: Some

Response
""""""""
:header: `HTTPRsp`_
:details: field form original packet that is being responded to
:body: server response on request
:blocker_id: Some(copied from original HTTPReq packet)

ModuleError
~~~~~~~~~~~
This is a packet meant for forwarding errors that are encountered
by the module to the client for debugging and logging purposes.

:details: name of error type
:body: further description of encountered error
:blocker_id: None

Store
~~~~~
This is a persistent store command.

:details: data type and unique identifier (for a detailed
	  explaination see below)
:body: contains data that is to be stored
:blocker_id: Some

When storing an ``OlmAccount``, details will look like
this: ``account/<account_id>``.
So the substring until the first forward slash is
encountered is the type of data and everything that
follows the unique identifier.
In a traditional relational database, each data type
could be stored in its own table, identified by the
name of the data type. The unique identifier of the
data could be used as the primary key.

Response
""""""""
:header: `StorageRsp`_
:details: empty string
:body: empty string
:blocker_id: Some (copied from original Store packet)

Load
~~~~
Analogous to *Store*, however it loads previously stored data
and sends it to the module.

:details: data type and unique identifier (for a detailed
	  explaination see Store)
:body: empty string
:blocker_id: Some

Response
""""""""
:header: `StorageRsp`_
:details: empty string
:body: return data (explained below)
:blocker_id: Some (copied from original Store packet)

The return data is the data that was stored under the identifier
of the original Load packet. If no data is stored under that
identifier, the ``body`` should be an empty string.

RequestInfo
~~~~~~~~~~~
Used to request information from the client, that the module needs.

:details: Reference code for the information required (see table below)
:body: dependent on reference code (see table below)
:blocker_id: Some

Information types
"""""""""""""""""
The names in the body column correspond to the structs that can be found
in ``metaolm::json_objects::request_info``. They are to be (de)searilised
using serde_json.

+-----------------+-----------------------------------+---------------+
|Reference code   |Description                        |Body           |
+=================+===================================+===============+
|ROOMS_WITH_E2EE  |list of rooms that have end-to-end |empty string   |
|                 |encryption enabled                 |               |
+-----------------+-----------------------------------+---------------+
|USERS_FOR_ROOM   |list of users in the room,         |UsersForRoom   |
|                 |specified in the packet's body     |               |
+-----------------+-----------------------------------+---------------+
|NEXT_TXNID       |get the next valid transaction ID  |empty string   |
|                 |from the client                    |               |
+-----------------+-----------------------------------+---------------+

Response
""""""""
:header: `ProvideInfo`_
:details: copied from original packet
:body: provided information
:blocker_id: Some (copied from original packet)

ActionRsp
~~~~~~~~~
Is used for responding to `ActionReq`_ packets originating from the client.
Check its Response section, to see how they should be constructed.

SelfInfo
~~~~~~~~

This is a packet type that is for internal use within metaolm only.
If a packet of this type ever reaches the client - that's a bug.


C2M Packets
-----------

M2C packets have a ``header``, ``body`` and
``blocker_id`` attributes.

:header: main differentiator for the functionality of a packet
:details: specific details for a packet, that is better left separated from
	  the body
:body: contains main information of the packet
:blocker_id: optionally defined ID of the event this event is blocking

The following packet types exist and are to be understood as follows:

HTTPRsp
~~~~~~~

Is used for responding to HTTPReq packets originating from the module.
Check the response for the `HTTPReq`_ packet for details on how to send
this packet.

ProvideInfo
~~~~~~~~~~~
Provides information for the module.

:details: information type
:body: provided information
:blocker_id: None

.. provide list of information types used for exchange

ActionReq
~~~~~~~~~
Requests a specific action from the module.

:details: reference code for action that is to be taken (see table below)
:body: information that is required for the specific action
       (may be used or empty, please look at the table below)
:blocker_id: Some

+------------------------+--------------------------------------+-----------------+
|Reference Code          |Description                           |Body             |
+========================+======================================+=================+
|ENABLE_ENCRYPTION       |Requests the room event for enabling  |empty String     |
|                        |encryption with recommended defaults  |                 |
+------------------------+--------------------------------------+-----------------+
|FORWARD_ENCRYPTED_EVENT |Requests the decryption of the        |encrypted Matrix |
|                        |forwarded encrypted event in the body |event            |
+------------------------+--------------------------------------+-----------------+

Response
""""""""
:header: `ActionRsp`_
:details: copied from original packet
:body: provided information, if any (see table below)
:blocker_id: Some (copied from original packet)

+-------------------+-------------------------------------+
|Reference Code     |Body                                 |
+===================+=====================================+
|ENABLE_ENCRYPTION  |JSON formatted Matrix event for      |
|                   |enabling E2EE in a Matrix room       |
+-------------------+-------------------------------------+


StorageRsp
~~~~~~~~~~

Is used for responding to `Load`_/`Store`_ packets originating from the module.
Check the responses for the Load/Store packet types on how to handle them
in each case.

TerminateModule
~~~~~~~~~~~~~~~

For ending the module's main loop.

:details: empty string
:body: empty string
:blocker_id: None

It is recommended to try to receive on the client's receiving channel
and wait for a ``RecvError``, as a confirmation that the module's thread
has been terminated properly.
