Client-side implementation of Metaolm
=====================================

.. warning:: This is an early draft document.
	     Everything here will be subject to change.

.. contents::

Introduction
------------
This is a guide for implementing Metaolm for a Matrix client to enable
end-to-end encrypted communication.

In order to properly understand this document, it is recommended that
you have read the introductory paragraphs of the
"Module/Client Communication" document.

Visualised example
------------------
To visually demonstrate how the communication between all involved actors
is supposed to work, here is a UML sequence diagram detailing the
initial usage of Metaolm with OlmAccount generation, one time key uploading
and a TerminateReq at the end:

.. image:: init_sequence.png

Enabling E2EE in a room
-----------------------
In order to enable E2EE in a room, the client will have to send a
``m.room.encryption`` state event.

It is possible to query Metaolm for such a state event with a packet
of type ``C2MPacketType::ActionReq``. The details field must be
``ENABLE_ENCRYPTION``. See the "Module/Client Communication" document
for further reference. This will return an ``m.room.encryption`` state
event with recommended defaults.

While it is recommended to use Metaolm for getting the state event,
it is possible for the client to send this event on its own
however, without asking the client.
In case fine control over specific parameters for enabling E2EE is
desired, this kind of manual workaround is necessary.

Details on the state event can be found in the `Matrix specification`_.

Handling encrypted events
-------------------------

Matrix events of type ``m.room.encrypted`` have to be forwarded to
Metaolm for decryption. For doing that the client is supposed to send
a packet of type ``C2MPacketType::ActionReq`` with ``FORWARD_ENCRYPTED_EVENT``
as the details field.

The hopefully decrypted event will be sent as a packet of type
``C2MPacketType::ActionRsp`` with the corresponding blocker ID.
In case any errors where encountered when decrypting, the packet will be of type
``C2MPacketType::ModuleError``.

Please note that for decryption additional information might be necessary, that
the module currently doesn't have. As a result a decrypted event may not be
sent immediately, but instead HTTP or storage requests.

.. _Matrix specification: https://matrix.org/docs/spec/client_server/r0.4.0.html#m-room-encryption
