// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use metaolm::builder::MetaolmBuilder;
use metaolm::endpoints;
use metaolm::json_objects::device::SignedDeviceKeys;
use metaolm::json_objects::event::*;
use metaolm::json_objects::misc::MegolmPlaintextPacketBody;
use metaolm::json_objects::olm::OlmIdentityKeys;
use metaolm::json_objects::otk::{OneTimeKeyCount, OneTimeKeyCountBody, OneTimeKeys};
use metaolm::packet::*;
use olm_rs::{
    account::OlmAccount, outbound_group_session::OlmOutboundGroupSession, session::OlmSession,
    utility::OlmUtility, PicklingMode,
};
use std::collections::HashMap;
use std::sync::mpsc::channel;
use std::thread;

#[test]
fn empty_init() {
    // Channel initialisation

    // Client <== Module
    let (csender, creceiver) = channel();
    // Client ==> Module
    let (msender, mreceiver) = channel();

    thread::spawn(move || {
        let metaolm_builder = MetaolmBuilder::new();
        let mut metaolm = metaolm_builder
            .with_device_id("some_id")
            .with_user_id("some_user_id")
            .connected(csender, mreceiver)
            .finish()
            .unwrap();

        metaolm.loop_start();
    });

    let load_req = creceiver.recv().unwrap();

    assert_eq!(
        load_req.header,
        OutPacketType::Load(StorageDesc::Account {
            device_id: "some_id".to_string()
        })
    );

    let response = InPacket {
        header: InPacketType::StorageRsp,
        body: String::new(),
        blocker_id: load_req.blocker_id,
    };

    msender.send(response).unwrap();

    let store_req = creceiver.recv().unwrap();

    assert_eq!(
        store_req.header,
        OutPacketType::Store(StorageDesc::Account {
            device_id: "some_id".to_string()
        })
    );

    // OlmAccount encoded as valid base64?
    base64::decode(&store_req.body).unwrap();
    // get OlmAccount for testing purposes
    let account = OlmAccount::unpickle(store_req.body, PicklingMode::Unencrypted).unwrap();

    msender
        .send(InPacket {
            header: InPacketType::StorageRsp,
            body: String::new(),
            blocker_id: Some(store_req.blocker_id.unwrap()),
        })
        .unwrap();

    let device_keys_req = creceiver.recv().unwrap();
    assert_eq!(
        OutPacketType::HTTPReq(HTTPReqType::Post, endpoints::UPLOAD_KEYS.to_string()),
        device_keys_req.header
    );

    // get public signing key
    let public_key_pair: OlmIdentityKeys = serde_json::from_str(&account.identity_keys()).unwrap();
    let sign_key = public_key_pair.ed25519;
    let utility = OlmUtility::new();

    // get device keys signature
    let device_keys_json: SignedDeviceKeys = serde_json::from_str(&device_keys_req.body).unwrap();
    let signatures_map = device_keys_json.signatures.values().next().unwrap();
    let mut signature_str = signatures_map.signatures.values().next().unwrap().clone();

    // check signature of device keys
    assert!(utility
        .ed25519_verify(
            &sign_key,
            &serde_json::to_string(&device_keys_json.get_device_keys_only()).unwrap(),
            &mut signature_str
        )
        .unwrap());

    let otk_count_req = creceiver.recv().unwrap();
    assert_eq!(
        OutPacketType::HTTPReq(HTTPReqType::Post, endpoints::UPLOAD_KEYS.to_string()),
        otk_count_req.header
    );
    assert_eq!(String::from("{}"), otk_count_req.body);

    let otk_count_rsp = InPacket {
        header: InPacketType::HTTPRsp(HTTPReqType::Post, String::new()),
        body: String::from(
            "{\"one_time_key_counts\":{\"curve25519\": 10,\"signed_curve25519\":20}}",
        ),
        blocker_id: otk_count_req.blocker_id,
    };

    msender.send(otk_count_rsp).unwrap();

    let otk_upload_req = creceiver.recv().unwrap();
    assert_eq!(
        OutPacketType::HTTPReq(HTTPReqType::Post, endpoints::UPLOAD_KEYS.to_string()),
        otk_upload_req.header
    );

    // check for defined "one_time_keys" key
    let otk_upload_json: serde_json::Value = serde_json::from_str(&otk_upload_req.body).unwrap();
    assert!(otk_upload_json["one_time_keys"].is_object());

    let terminate_req = InPacket {
        header: InPacketType::TerminateModule,
        body: String::new(),
        blocker_id: None,
    };

    // check response for request to enable encryption
    let enable_encryption_blocker = 1;
    msender
        .send(InPacket {
            header: InPacketType::ActionReq(ActionType::EnableEncryption),
            body: String::new(),
            blocker_id: Some(enable_encryption_blocker),
        })
        .unwrap();

    let enable_encryption_rsp = creceiver.recv().unwrap();
    assert_eq!(
        OutPacketType::ActionRsp(ActionType::EnableEncryption),
        enable_encryption_rsp.header
    );

    // body should contain serialisable string
    {
        let _encryption: Encryption = serde_json::from_str(&enable_encryption_rsp.body).unwrap();
    }

    msender.send(terminate_req).unwrap();

    // waiting for channel to hang up
    match creceiver.recv() {
        Err(_) => {}
        _ => {
            panic!();
        }
    }
}

#[test]
fn known_account_init() {
    // Channel initialisation

    // Client <== Module
    let (csender, creceiver) = channel();
    // Client ==> Module
    let (msender, mreceiver) = channel();

    thread::spawn(move || {
        let metaolm_builder = MetaolmBuilder::new();
        metaolm_builder
            .with_device_id("some_id")
            .with_user_id("some_user_id")
            .connected(csender, mreceiver)
            .finish()
            .unwrap();
    });

    let initial_message = creceiver.recv().unwrap();

    let response = InPacket{
        header: InPacketType::StorageRsp,
        body: String::from("9Mfk9/6oShETGfYBzdnbzGdsiVjqlQwBU0aFJa6AUYgnizgAGJAzstDHpcGA3NXuXHPnKIQN0Kq9itC6P7xrXJ0i6VevDwxQBNgAC61of2CdIl/ghI5Dua5uWsO25BMY1mVXiTA8jnINmQsKzba8gb9kw/yd/zM3BxVaZ0BgRyHoSWc3SXVlnol0isUcB+OtjznpgmSikCf3+17cK4+Eyke230L0cEjseCp0EaMUNW7Gn+rovmQYGQ"),
        blocker_id: initial_message.blocker_id,
    };

    msender.send(response).unwrap();

    let terminate_req = InPacket {
        header: InPacketType::TerminateModule,
        body: String::new(),
        blocker_id: None,
    };

    msender.send(terminate_req).unwrap();
    // waiting for channel to hang up
    match creceiver.recv() {
        Err(_) => {}
        _ => {
            panic!();
        }
    }
}

#[test]
pub fn olm_decryption_test() {
    // Channel initialisation

    // Client <== Module
    let (csender, creceiver) = channel();
    // Client ==> Module
    let (msender, mreceiver) = channel();

    thread::spawn(move || {
        let metaolm_builder = MetaolmBuilder::new();
        let mut metaolm = metaolm_builder
            .with_device_id("some_id")
            .with_user_id("some_user_id")
            .connected(csender, mreceiver)
            .finish()
            .unwrap();

        metaolm.loop_start();
    });

    let initial_message = creceiver.recv().unwrap();

    let account = OlmAccount::new();

    let response = InPacket {
        header: InPacketType::StorageRsp,
        body: account.pickle(PicklingMode::Unencrypted),
        blocker_id: initial_message.blocker_id,
    };

    msender.send(response).unwrap();

    // request for amount of OTKs that currently are registered
    // on the Matrix server
    let otk_count_req = creceiver.recv().unwrap();
    // mocked answer of 0
    if let OutPacketType::HTTPReq(t, u) = otk_count_req.header {
        msender
            .send(InPacket {
                header: InPacketType::HTTPRsp(t, u),
                body: serde_json::to_string(&OneTimeKeyCount {
                    one_time_key_counts: OneTimeKeyCountBody {
                        signed_curve25519: 0,
                    },
                })
                .unwrap(),
                blocker_id: otk_count_req.blocker_id,
            })
            .unwrap();
    }

    // upload request for OTK to the Matrix server
    let otk_upload_req = creceiver.recv().unwrap();

    msender
        .send(InPacket {
            header: InPacketType::StorageRsp,
            body: String::new(),
            blocker_id: otk_upload_req.blocker_id,
        })
        .unwrap();

    let _account_store_req = creceiver.recv().unwrap();

    // extract on OTK for later use
    let otks: OneTimeKeys = serde_json::from_str(&otk_upload_req.body).unwrap();
    let otk = otks.one_time_keys.values().next().unwrap();

    // extract fingerprint key of our device key
    let curve25519_key;
    {
        let public_key_pair: OlmIdentityKeys =
            serde_json::from_str(&account.identity_keys()).unwrap();
        curve25519_key = public_key_pair.curve25519;
    }

    // other account, used as a communication partner to test encryption
    let account_b = OlmAccount::new();

    let curve25519_key_b;
    {
        let public_key_pair: OlmIdentityKeys =
            serde_json::from_str(&account_b.identity_keys()).unwrap();
        curve25519_key_b = public_key_pair.curve25519;
    }

    // create a new outbound session for account_b
    let outbound =
        OlmSession::create_outbound_session(&account_b, &curve25519_key, &otk.key).unwrap();

    let olm_plaintext = OlmPlaintext {
        r#type: String::new(),
        content: String::from("Hello world!"),
        sender: String::from("account_b"),
        sender_device: String::from("device_b"),
        keys: Key {
            ed25519: curve25519_key_b.clone(),
        },
        recipient: String::from("some_user_id"),
        recipient_keys: Key {
            ed25519: curve25519_key.clone(),
        },
    };
    let pre_key = outbound.encrypt(&serde_json::to_string(&olm_plaintext).unwrap());

    let ciphertext_info = CiphertextInfo {
        body: pre_key,
        r#type: 0,
    };
    let mut ciphertext_map = HashMap::new();
    ciphertext_map.insert(curve25519_key.clone(), ciphertext_info);

    let encrypted_event = OlmEncrypted {
        room_id: String::from("room"),
        sender: String::from("account_b"),
        content: OlmEncryptedContent {
            algorithm: String::from("m.olm.v1.curve25519-aes-sha2"),
            ciphertext: ciphertext_map,
            sender_key: curve25519_key_b.clone(),
        },
        r#type: "m.room.encrypted".to_string(),
    };

    msender
        .send(InPacket {
            header: InPacketType::ActionReq(ActionType::ForwardEncryptedEvent),
            body: serde_json::to_string(&encrypted_event).unwrap(),
            blocker_id: Some(1),
        })
        .unwrap();

    let load_req = creceiver.recv().unwrap();

    msender
        .send(InPacket {
            header: InPacketType::StorageRsp,
            body: String::new(),
            blocker_id: load_req.blocker_id,
        })
        .unwrap();

    let _session_store_req = creceiver.recv().unwrap();

    let account_store_req = creceiver.recv().unwrap();

    assert_ne!(
        account.pickle(PicklingMode::Unencrypted),
        account_store_req.body
    );

    let plaintext_rsp = creceiver.recv().unwrap();
    assert_eq!(
        serde_json::to_string(&olm_plaintext).unwrap(),
        plaintext_rsp.body
    );

    let terminate_req = InPacket {
        header: InPacketType::TerminateModule,
        body: String::new(),
        blocker_id: None,
    };

    msender.send(terminate_req).unwrap();
    // waiting for channel to hang up
    match creceiver.recv() {
        Err(_) => {}
        _ => {
            panic!();
        }
    }
}

#[test]
pub fn megolm_decryption_test() {
    // Channel initialisation

    // Client <== Module
    let (csender, creceiver) = channel();
    // Client ==> Module
    let (msender, mreceiver) = channel();

    thread::spawn(move || {
        let metaolm_builder = MetaolmBuilder::new();
        let mut metaolm = metaolm_builder
            .with_device_id("device_id_a")
            .with_user_id("@a_user_id:example.org")
            .connected(csender, mreceiver)
            .finish()
            .unwrap();

        metaolm.loop_start();
    });

    let initial_message = creceiver.recv().unwrap();

    let account = OlmAccount::new();

    let response = InPacket {
        header: InPacketType::StorageRsp,
        body: account.pickle(PicklingMode::Unencrypted),
        blocker_id: initial_message.blocker_id,
    };

    msender.send(response).unwrap();

    // request for amount of OTKs that currently are registered
    // on the Matrix server
    let otk_count_req = creceiver.recv().unwrap();
    // mocked answer of 0

    if let OutPacketType::HTTPReq(t, u) = otk_count_req.header {
        msender
            .send(InPacket {
                header: InPacketType::HTTPRsp(t, u),
                body: serde_json::to_string(&OneTimeKeyCount {
                    one_time_key_counts: OneTimeKeyCountBody {
                        signed_curve25519: 0,
                    },
                })
                .unwrap(),
                blocker_id: otk_count_req.blocker_id,
            })
            .unwrap();
    }

    // upload request for OTK to the Matrix server
    let otk_upload_req = creceiver.recv().unwrap();

    msender
        .send(InPacket {
            header: InPacketType::StorageRsp,
            body: String::new(),
            blocker_id: otk_upload_req.blocker_id,
        })
        .unwrap();

    let _account_store_req = creceiver.recv().unwrap();

    // extract on OTK for later use
    let otks: OneTimeKeys = serde_json::from_str(&otk_upload_req.body).unwrap();
    let otk = otks.one_time_keys.values().next().unwrap();

    // extract fingerprint key of our device key
    let curve25519_key;
    {
        let public_key_pair: OlmIdentityKeys =
            serde_json::from_str(&account.identity_keys()).unwrap();
        curve25519_key = public_key_pair.curve25519;
    }

    // other account, used as a communication partner
    let account_b = OlmAccount::new();

    let curve25519_key_b;
    {
        let public_key_pair: OlmIdentityKeys =
            serde_json::from_str(&account_b.identity_keys()).unwrap();
        curve25519_key_b = public_key_pair.curve25519;
    }

    // create a new outbound session for account_b
    let outbound =
        OlmSession::create_outbound_session(&account_b, &curve25519_key, &otk.key).unwrap();

    // create outbound Megolm session
    let outbound_group = OlmOutboundGroupSession::new();
    // retrieve session ID, current ratchet key and session index
    let outbound_group_session_id = outbound_group.session_id();
    let outbound_group_session_key = outbound_group.session_key();

    // build m.room_key event
    let room_key_event = RoomKey {
        content: RoomKeyContent {
            algorithm: "m.megolm.v1.aes-sha2".to_string(),
            room_id: "!abc:example.org".to_string(),
            session_id: outbound_group_session_id.clone(),
            session_key: outbound_group_session_key,
        },
        r#type: "m.room_key".to_string(),
    };

    // encrypt m.room_key event using Olm
    let olm_plaintext = OlmPlaintext {
        r#type: String::new(),
        content: serde_json::to_string(&room_key_event).unwrap(),
        sender: "@b_user_id:example.org".to_string(),
        sender_device: "device_b".to_string(),
        keys: Key {
            ed25519: curve25519_key_b.clone(),
        },
        recipient: "@a_user_id:example.org".to_string(),
        recipient_keys: Key {
            ed25519: curve25519_key.clone(),
        },
    };

    let pre_key = outbound.encrypt(&serde_json::to_string(&olm_plaintext).unwrap());

    let ciphertext_info = CiphertextInfo {
        body: pre_key,
        r#type: 0,
    };

    let mut ciphertext_map = HashMap::new();
    ciphertext_map.insert(curve25519_key.clone(), ciphertext_info);

    let encrypted_event = OlmEncrypted {
        room_id: "!abc:example.org".to_string(),
        sender: "@b_user_id:example.org".to_string(),
        content: OlmEncryptedContent {
            algorithm: "m.olm.v1.curve25519-aes-sha2".to_string(),
            ciphertext: ciphertext_map,
            sender_key: curve25519_key_b.clone(),
        },
        r#type: "m.room.encrypted".to_string(),
    };

    msender
        .send(InPacket {
            header: InPacketType::ActionReq(ActionType::ForwardEncryptedEvent),
            body: serde_json::to_string(&encrypted_event).unwrap(),
            blocker_id: Some(1),
        })
        .unwrap();

    let load_req = creceiver.recv().unwrap();

    msender
        .send(InPacket {
            header: InPacketType::StorageRsp,
            body: String::new(),
            blocker_id: load_req.blocker_id,
        })
        .unwrap();

    creceiver.recv().unwrap(); // session store request
    creceiver.recv().unwrap(); // account store request

    let session_req = creceiver.recv().unwrap(); // megolm session load request
    match session_req.header {
        // check for correct storage descriptor
        OutPacketType::Load(StorageDesc::InboundMegolmSession {
            room_id: _,
            sender_key: _,
            session_id: _,
        }) => {}
        _ => panic!(),
    }
    assert_eq!(String::new(), session_req.body);

    // answer load request with empty storage response
    msender
        .send(InPacket {
            header: InPacketType::StorageRsp,
            body: String::new(),
            blocker_id: session_req.blocker_id,
        })
        .unwrap();

    // the response to us forwarding that encrypted m.room_key event
    let action_rsp = creceiver.recv().unwrap();
    assert_eq!(
        OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
        action_rsp.header
    );
    assert_eq!(String::new(), action_rsp.body);
    assert_eq!(Some(1), action_rsp.blocker_id);

    // store request for megolm session
    let megolm_store_packet = creceiver.recv().unwrap();
    match megolm_store_packet.header {
        OutPacketType::Store(_) => {}
        _ => panic!(),
    }

    let megolm_encrypted = MegolmEncrypted {
        content: MegolmContent {
            algorithm: "m.megolm.v1.aes-sha2".to_string(),
            ciphertext: outbound_group.encrypt("This is our secret!".to_string()),
            sender_key: curve25519_key_b.clone(),
            device_id: "device_b".to_string(),
            session_id: outbound_group_session_id,
        },
        r#type: "m.room.encrypted".to_string(),
        event_id: "$1234:example.org".to_string(),
        origin_server_ts: 12345,
        room_id: "!abc:example.org".to_string(),
    };

    let megolm_encrypted_packet = InPacket {
        header: InPacketType::ActionReq(ActionType::ForwardEncryptedEvent),
        body: serde_json::to_string(&megolm_encrypted).unwrap(),
        blocker_id: Some(2),
    };

    msender.send(megolm_encrypted_packet).unwrap();

    // megolm decrypted plaintext
    let megolm_plaintext_packet = creceiver.recv().unwrap();
    assert_eq!(
        OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
        megolm_plaintext_packet.header
    );
    assert_eq!(Some(2), megolm_plaintext_packet.blocker_id);
    let megolm_plaintext: MegolmPlaintextPacketBody =
        serde_json::from_str(&megolm_plaintext_packet.body).unwrap();
    // FIXME: refactor MegolmPlaintextPacketBody to m.room.message
    let plaintext = megolm_plaintext.content.iter().next().unwrap();
    assert_eq!("This is our secret!", plaintext.body.as_str());

    let terminate_req = InPacket {
        header: InPacketType::TerminateModule,
        body: String::new(),
        blocker_id: None,
    };

    msender.send(terminate_req).unwrap();
    // waiting for channel to hang up
    match creceiver.recv() {
        Err(_) => {}
        _ => {
            panic!();
        }
    }
}
