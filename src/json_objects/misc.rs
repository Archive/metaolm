// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use super::event::MegolmEncrypted;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone)]
pub struct Signatures {
    #[serde(flatten)]
    pub signatures: HashMap<String, String>,
}

#[derive(Serialize, Deserialize)]
pub struct MegolmPlaintextPacketBody {
    pub content: Result<MegolmPlaintextPacketBodyContentOk, MegolmPlaintextPacketBodyContentErr>,
    pub origin_server_ts: u64,
    pub room_id: String,
    pub r#type: String,
}

#[derive(Serialize, Deserialize)]
pub struct MegolmPlaintextPacketBodyContentOk {
    pub body: String,
}

#[derive(Serialize, Deserialize)]
pub struct MegolmPlaintextPacketBodyContentErr {
    pub error: String,
}

impl Signatures {
    pub fn new(device_id: &str, signature: &str) -> Self {
        let mut signatures = HashMap::new();
        signatures.insert(format!("ed25519:{}", device_id), String::from(signature));
        Signatures { signatures }
    }
}

impl MegolmPlaintextPacketBody {
    pub fn from_megolm_encrypted(megolm_encrypted: &MegolmEncrypted, plaintext: &str) -> Self {
        MegolmPlaintextPacketBody {
            content: Ok(MegolmPlaintextPacketBodyContentOk {
                body: plaintext.to_string(),
            }),
            origin_server_ts: megolm_encrypted.origin_server_ts,
            room_id: megolm_encrypted.room_id.clone(),
            r#type: "m.room.message".to_string(),
        }
    }
}
