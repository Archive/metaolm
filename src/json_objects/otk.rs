// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use super::misc::Signatures;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Default)]
pub struct OneTimeKeys {
    pub one_time_keys: HashMap<String, SignedOneTimeKey>,
}

#[derive(Serialize, Deserialize, Default)]
pub struct OneTimeKeyQuery {
    pub one_time_keys: HashMap<String, HashMap<String, String>>,
}

#[derive(Serialize, Deserialize)]
pub struct OneTimeKeyQueryResponse {
    // <user_id> -> <device_id> -> signed_curve25519:<device_id>
    one_time_keys: HashMap<String, HashMap<String, HashMap<String, SignedOneTimeKey>>>,
}

#[derive(Serialize, Deserialize)]
pub struct AccountOneTimeKeys {
    pub curve25519: HashMap<String, String>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SignedOneTimeKey {
    pub key: String,
    pub signatures: HashMap<String, Signatures>,
}

#[derive(Serialize, Deserialize)]
pub struct OneTimeKey {
    pub key: String,
}

#[derive(Serialize, Deserialize)]
pub struct OneTimeKeyCount {
    pub one_time_key_counts: OneTimeKeyCountBody,
}

#[derive(Serialize, Deserialize)]
pub struct OneTimeKeyCountBody {
    pub signed_curve25519: u64,
}

impl OneTimeKeys {
    pub fn new() -> Self {
        OneTimeKeys {
            one_time_keys: HashMap::new(),
        }
    }

    pub fn add_key(&mut self, key_id: &str, one_time_key: SignedOneTimeKey) {
        self.one_time_keys
            .insert(format!("signed_curve25519:{}", key_id), one_time_key);
    }
}

impl SignedOneTimeKey {
    pub fn new(curve25519_key: &str) -> Self {
        SignedOneTimeKey {
            key: String::from(curve25519_key),
            signatures: HashMap::new(),
        }
    }

    pub fn get_otk_only(&self) -> OneTimeKey {
        OneTimeKey {
            key: self.key.clone(),
        }
    }

    pub fn set_signature(&mut self, user_id: &str, device_id: &str, signature: &str) {
        let user_sig = Signatures::new(device_id, signature);
        self.signatures.insert(String::from(user_id), user_sig);
    }
}

impl OneTimeKeyQuery {
    pub fn new(user_id: String, device_id: String) -> Self {
        let mut new_instance = Self {
            one_time_keys: HashMap::new(),
        };

        let mut key_map = HashMap::new();
        key_map.insert(device_id, "signed_curve25519".to_string());
        new_instance.one_time_keys.insert(user_id, key_map);

        new_instance
    }
}

impl OneTimeKeyQueryResponse {
    pub fn extract_otk(&self) -> SignedOneTimeKey {
        // FIXME: oof
        (*self
            .one_time_keys
            .iter()
            .next()
            .unwrap()
            .1
            .iter()
            .next()
            .unwrap()
            .1
            .iter()
            .next()
            .unwrap()
            .1)
            .clone()
    }

    pub fn get_user_id(&self) -> String {
        self.one_time_keys.iter().next().unwrap().0.clone()
    }

    pub fn get_device_id(&self) -> String {
        self.one_time_keys
            .iter()
            .next()
            .unwrap()
            .1
            .iter()
            .next()
            .unwrap()
            .0
            .to_string()
    }
}
