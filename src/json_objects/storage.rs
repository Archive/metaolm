// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::session_cache::OutboundGroupSessionWithInfo;
use olm_rs::{outbound_group_session::OlmOutboundGroupSession, PicklingMode};
use serde_derive::{Deserialize, Serialize};

/// An inbound Megolm session with the associated ed25519
/// fingerprint key for verifying received messages.
/// Copy of metaolm::session_cache::InboundGroupSessionWithKey
/// for the purpose of storing the session as a pickled String.
/// Information regarding message indeces are stored separately.
#[derive(Serialize, Deserialize)]
pub struct InboundGroupSessionWithKeyForStorage {
    pub session: String,
    pub ed25519: String,
}

/// An outbound Megolm session with otherwise needed information.
/// Rotation parameters being both the desired max. lifetime and
/// max. usage amounts.
/// To decide if a sesison is outdated, we store the creation time
/// an the amount the session was used as well.
#[derive(Serialize, Deserialize)]
pub struct OutboundGroupSessionWithInfoForStorage {
    pub session: String,
    pub rotation_period_ms: u64,
    pub rotation_period_msgs: u32,
    // time the session was created in UNIX time
    pub created_at: u64,
    pub msgs_sent: u32,
}

impl OutboundGroupSessionWithInfoForStorage {
    pub fn from_active(session_info: &OutboundGroupSessionWithInfo) -> Self {
        Self {
            session: session_info.session.pickle(PicklingMode::Unencrypted),
            rotation_period_ms: session_info.rotation_period_ms,
            rotation_period_msgs: session_info.rotation_period_msgs,
            created_at: session_info.created_at,
            msgs_sent: session_info.msgs_sent,
        }
    }

    pub fn into_active(&self) -> OutboundGroupSessionWithInfo {
        OutboundGroupSessionWithInfo {
            session: OlmOutboundGroupSession::unpickle(
                self.session.clone(),
                PicklingMode::Unencrypted,
            )
            .unwrap(),
            rotation_period_ms: self.rotation_period_ms,
            rotation_period_msgs: self.rotation_period_msgs,
            created_at: self.created_at,
            msgs_sent: self.msgs_sent,
        }
    }
}
