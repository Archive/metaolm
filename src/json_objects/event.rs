// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Encryption {
    r#type: String,
    state_key: String,
    content: EncryptionContent,
}

#[derive(Serialize, Deserialize)]
struct EncryptionContent {
    algorithm: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    rotation_period_ms: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    rotation_period_msgs: Option<u64>,
}

#[derive(Serialize, Deserialize)]
pub struct RoomEncrypted {
    pub content: RoomEncryptedContent,
    pub r#type: String,
    pub room_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct RoomEncryptedContent {
    pub algorithm: String,
    pub ciphertext: serde_json::Value,
    pub sender_key: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub device_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub session_id: Option<String>,
}

/// Encrypted room event before being sent to the server with
/// `/rooms/<room_id>/send/m.room.encrypted/<txn_id>`.
#[derive(Serialize, Deserialize)]
pub struct RoomEncryptedForSending {
    pub algorithm: String,
    pub sender_key: String,
    pub ciphertext: String,
    pub session_id: String,
    pub device_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct OlmEncrypted {
    pub room_id: String,
    pub sender: String,
    pub content: OlmEncryptedContent,
    pub r#type: String,
}

#[derive(Serialize, Deserialize)]
pub struct OlmEncryptedContent {
    pub algorithm: String,
    pub ciphertext: HashMap<String, CiphertextInfo>,
    pub sender_key: String,
}

#[derive(Serialize, Deserialize)]
pub struct CiphertextInfo {
    pub body: String,
    pub r#type: u64,
}

#[derive(Serialize, Deserialize)]
pub struct OlmPlaintext {
    pub r#type: String,
    pub content: String,
    pub sender: String,
    pub sender_device: String,
    pub keys: Key,
    pub recipient: String,
    pub recipient_keys: Key,
}

/// Taken from https://matrix.org/docs/spec/client_server/r0.4.0.html#m-olm-v1-curve25519-aes-sha2
#[derive(Serialize, Deserialize)]
pub struct OlmPayload {
    pub r#type: String,
    pub content: String,
    pub sender: String,
    pub recipient: String,
    pub recipient_keys: HashMap<String, String>,
    pub keys: HashMap<String, String>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MegolmEncrypted {
    pub content: MegolmContent,
    pub r#type: String,
    pub event_id: String,
    pub origin_server_ts: u64,
    pub room_id: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MegolmContent {
    pub algorithm: String,
    pub ciphertext: String,
    pub sender_key: String,
    pub device_id: String,
    pub session_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct MegolmEncryptedPayload {
    pub r#type: String,
    pub content: String,
    pub room_id: String,
}

/// Struct for Matrix's `m.room.message`
#[derive(Serialize, Deserialize, Clone)]
pub struct RoomMessage {
    pub content: RoomMessageContent,
    pub r#type: String,
    pub room_id: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct RoomMessageContent {
    pub body: String,
    pub msgtype: String,
}

#[derive(Serialize, Deserialize)]
pub struct Key {
    pub ed25519: String,
}

#[derive(Serialize, Deserialize)]
pub struct RoomKey {
    pub content: RoomKeyContent,
    pub r#type: String,
}

#[derive(Serialize, Deserialize)]
pub struct RoomKeyContent {
    pub algorithm: String,
    pub room_id: String,
    pub session_id: String,
    pub session_key: String,
}

impl Encryption {
    /// Creates a new Encryption event with the recommended values.
    pub fn new() -> Self {
        let encryption_content = EncryptionContent {
            algorithm: String::from("m.megolm.v1.aes-sha2"),
            rotation_period_ms: Some(604_800_000),
            rotation_period_msgs: Some(100),
        };

        Encryption {
            r#type: String::from("m.room.encryption"),
            state_key: String::new(),
            content: encryption_content,
        }
    }
}

impl Default for Encryption {
    fn default() -> Self {
        Self::new()
    }
}

impl OlmEncrypted {
    /// Gets the ciphertext info object, that is mapped to the
    /// supplied curve25519 key.
    pub fn get_ciphertext_info(&self, curve25519_key: &str) -> Option<&CiphertextInfo> {
        self.content.ciphertext.get(curve25519_key)
    }
}
