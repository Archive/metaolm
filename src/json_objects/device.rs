// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use super::misc::Signatures;
use super::olm::OlmIdentityKeys;
use serde_derive::{Deserialize, Serialize};
use serde_json;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Default)]
pub struct DeviceKeysRequest {
    device_keys: HashMap<String, Vec<String>>,
}

#[derive(Serialize, Deserialize)]
pub struct DeviceKeys {
    pub algorithm: Vec<String>,
    pub device_id: String,
    pub keys: OlmIdentityKeys,
    pub user_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct DeviceQueryResponse {
    pub failures: HashMap<String, serde_json::Value>,
    pub device_keys: HashMap<String, HashMap<String, SignedDeviceKeys>>,
}

#[derive(Serialize, Deserialize)]
pub struct SignedDeviceKeys {
    pub algorithm: Vec<String>,
    pub device_id: String,
    pub keys: OlmIdentityKeys,
    pub user_id: String,
    pub signatures: HashMap<String, Signatures>,
}

impl SignedDeviceKeys {
    pub fn new(device_id: &str, user_id: &str, identity_keys: OlmIdentityKeys) -> Self {
        SignedDeviceKeys {
            algorithm: [
                String::from("m.olm.v1.curve25519-aes-sha2"),
                String::from("m.megolm.v1.aes-sha2"),
            ]
            .to_vec(),
            device_id: String::from(device_id),
            keys: identity_keys,
            user_id: String::from(user_id),
            signatures: HashMap::new(),
        }
    }

    pub fn set_signature(&mut self, signature: &str) {
        let signatures = Signatures::new(&self.device_id, signature);
        self.signatures.insert(self.user_id.clone(), signatures);
    }

    /// Gets the device's own signature, if present.
    ///
    /// Useful for signature verification.
    pub fn get_own_signature(&self) -> Option<String> {
        if let Some(signatures) = self.signatures.get(&self.user_id) {
            if let Some(signature) = signatures
                .signatures
                .get(&format!("ed25519:{}", &self.device_id))
            {
                Some(signature.to_string())
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn get_device_keys_only(&self) -> DeviceKeys {
        let keys_json = serde_json::to_string(&self.keys).unwrap();
        DeviceKeys {
            algorithm: self.algorithm.clone(),
            device_id: self.device_id.clone(),
            keys: serde_json::from_str(&keys_json).unwrap(),
            user_id: self.user_id.clone(),
        }
    }
}

impl DeviceKeysRequest {
    pub fn new() -> Self {
        Self {
            device_keys: HashMap::new(),
        }
    }

    pub fn add_users(&mut self, users: Vec<String>) {
        for user in users {
            self.device_keys.insert(user, Vec::new());
        }
    }
}
