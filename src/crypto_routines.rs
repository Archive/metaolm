// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::BTreeMap;
use std::sync::mpsc::Sender;
use std::time::SystemTime;

use olm_rs::{
    account::OlmAccount,
    inbound_group_session::OlmInboundGroupSession,
    outbound_group_session::OlmOutboundGroupSession,
    session::{OlmMessageType, OlmSession},
    PicklingMode,
};

use ring::rand::{SecureRandom, SystemRandom};

use serde_json;

use crate::blocker::BlockerManager;
use crate::json_objects::{
    event::{
        CiphertextInfo, MegolmEncrypted, MegolmEncryptedPayload, OlmEncrypted, OlmPayload,
        OlmPlaintext, RoomEncrypted, RoomEncryptedContent, RoomKey,
    },
    misc::MegolmPlaintextPacketBody,
    olm::OlmIdentityKeys,
    self_info::{MegolmEncryptedBody, RoomKeyBody},
    storage::{InboundGroupSessionWithKeyForStorage, OutboundGroupSessionWithInfoForStorage},
    transport::C2MEncryptEventBody,
};
use crate::packet::{
    ActionType, Blocker, HTTPReqType, ModuleInfoReq, OutPacket, OutPacketType, StorageDesc,
};
use crate::session_cache::OutboundGroupSessionWithInfo;
use crate::session_cache::{InboundMegolmSessionIdentifier, SessionCache};

pub enum OlmDecryptResult {
    Plaintext(OlmPlaintext),
    NewSession(OlmSession),
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum OlmHandleType {
    FirstRun,
    SecondRun,
}

/// Decryption routine for events encrypted with the `m.olm.v1.curve25519-aes-sha2`
/// algorithm.
pub fn olm_decrypt(
    event: &str,
    account: &OlmAccount,
    session_cache: &SessionCache,
    user_id: &str,
) -> Result<OlmDecryptResult, String> {
    let encrypted_event: OlmEncrypted = match serde_json::from_str(&event) {
        Ok(x) => x,
        Err(_) => return Err(String::from("Error when serialising the received event")),
    };
    let sender_key = encrypted_event.content.sender_key.clone();
    let public_key_pair: OlmIdentityKeys = match serde_json::from_str(&account.identity_keys()) {
        Ok(x) => x,
        Err(_) => return Err(String::from("Error when serialising the public key pair")),
    };
    let curve25519_key = &public_key_pair.curve25519;
    // Extract specific ciphertext object for our device.
    let ciphertext_info_result = encrypted_event.get_ciphertext_info(&public_key_pair.curve25519);
    // Usually the received event should contain a ciphertext info object
    // specifically for our device.
    // In case it doesn't we have to send an error.
    let ciphertext_info = match ciphertext_info_result {
        Some(x) => x,
        None => {
            return Err("No ciphertext info object contained within received message".to_string());
        }
    };
    // Map message types against this enum for extra
    // safety and readability.
    let msg_type = match ciphertext_info.r#type {
        0 => OlmMessageType::PreKey,
        1 => OlmMessageType::Message,
        x => return Err(format!("Unsupported CiphertextInfo message type: {}", x)),
    };

    let cached_sessions_result = session_cache.get_cached_olm_sessions(&sender_key);
    let mut plaintext = None;

    // Here we'll attempt to find the session that was used for encrypting
    // the ciphertext info object among our cached sessions.
    if msg_type == OlmMessageType::PreKey && cached_sessions_result.is_some() {
        let cached_sessions = match cached_sessions_result {
            Some(x) => x,
            None => unreachable!(),
        };
        let mut matching_session = None;
        for session in cached_sessions {
            if session
                .matches_inbound_session(ciphertext_info.body.clone())
                .unwrap()
            {
                matching_session = Some(session);
                break;
            }
        }

        // In case of us finding a matching session for the ciphertext
        // in question, we can proceed to decrypting its contents.
        if let Some(session) = matching_session {
            let decrypt_result = session.decrypt(msg_type, ciphertext_info.body.clone());
            if decrypt_result.is_ok() {
                plaintext = Some(decrypt_result.unwrap());
            }
        }

    // This message was sent to an already established session, so we'll
    // have to look at every single cached session, to determine if we
    // can decrypt the ciphertext.
    } else if msg_type == OlmMessageType::Message && cached_sessions_result.is_some() {
        let cached_sessions = cached_sessions_result.unwrap();
        for session in cached_sessions {
            let decrypted_result = session.decrypt(msg_type, ciphertext_info.body.clone());
            if decrypted_result.is_ok() {
                plaintext = Some(decrypted_result.unwrap());
            }
        }
    }

    // Here we check if we went a path that has decrypted
    // the ciphertext.
    if plaintext.is_some() {
        let olm_plain: OlmPlaintext = match serde_json::from_str(&plaintext.unwrap()) {
            Ok(x) => x,
            Err(_) => return Err(
                "Error when trying to deserialise decrypted plaintext in olm decryption routine"
                    .to_string(),
            ),
        };

        // Verify properties in plaintext object
        let mut plaintext_check_error = None;

        if encrypted_event.sender != olm_plain.sender {
            plaintext_check_error = Some("sender");
        } else if olm_plain.recipient != *user_id {
            plaintext_check_error = Some("recipient");
        } else if olm_plain.keys.ed25519 != sender_key {
            plaintext_check_error = Some("sender_key");
        } else if olm_plain.recipient_keys.ed25519 != *curve25519_key {
            plaintext_check_error = Some("recipient_key");
        }

        match plaintext_check_error {
            Some(x) => Err(format!(
                "The following property does not match after decryption: {}",
                x
            )),
            None => Ok(OlmDecryptResult::Plaintext(olm_plain)),
        }
    } else if msg_type == OlmMessageType::PreKey {
        // Everything else has failed, so we'll try to establish a new session.
        let session = match OlmSession::create_inbound_session_from(
                    account,
                    &encrypted_event.content.sender_key,
                    ciphertext_info.body.clone(),
                ) {
                    Ok(x) => x,
                    Err(x) => return Err(format!("Error when trying to create a new inbound session for olm decryption routine: {:#?}", x))
                };

        match account.remove_one_time_keys(&session) {
            Ok(_) => {}
            Err(_) => return Err("Error when trying to remove OTK from olm account".to_string()),
        }

        Ok(OlmDecryptResult::NewSession(session))
    } else {
        Err(String::from("No available session to decypt this message"))
    }
}

/// Handles an event that was encrypted using the `m.olm.v1.curve25519-aes-sha2`
/// algorithm.
///
/// Instead of directly sending the `OutPacket`s that are generated by this routine,
/// they are returned and have to be sent manually afterwards.
pub(crate) fn handle_olm_event(
    event: OlmEncrypted,
    packet_blocker_id: Option<u32>,
    run_type: OlmHandleType,
    account: &OlmAccount,
    session_cache: &mut SessionCache,
    blocker_manager: &mut BlockerManager,
    blocked_items: &mut BTreeMap<u32, Blocker>,
    user_id: &str,
    device_id: &str,
) -> Vec<OutPacket> {
    let sender_key = &event.content.sender_key;

    // The list of packets we are going to return for sending to the client.
    let mut packet_list = Vec::new();

    // First we have to find out, wether or not we have
    // the session in question cached, to reduce
    // unnecessary communication with the client.
    if !session_cache.olm_session_cached(&sender_key) && run_type == OlmHandleType::FirstRun {
        // If we don't, we have to ask the client
        // about the potentially exported session data.
        let load_blocker_id = blocker_manager.next_blocker_id();
        packet_list.push(OutPacket {
            header: OutPacketType::Load(StorageDesc::OlmSession {
                device_id: device_id.to_string(),
                sender_key: sender_key.to_string(),
            }),
            body: String::new(),
            blocker_id: Some(load_blocker_id),
        });

        let self_info_decrypt = Blocker::OlmDecrypt(event, packet_blocker_id.unwrap());
        blocked_items.insert(load_blocker_id, self_info_decrypt);
    } else {
        // If we do, we can immediately proceed
        // towards decrypting the received event.
        match olm_decrypt(
            &serde_json::to_string(&event).unwrap(),
            account,
            session_cache,
            user_id,
        ) {
            Ok(OlmDecryptResult::Plaintext(plaintext)) => {
                // check for m.room_key event
                if check_for_room_key_event_and_handle(
                    &plaintext.content,
                    &event.room_id,
                    &event.content.sender_key,
                    &plaintext.keys.ed25519,
                    session_cache,
                    blocker_manager,
                    blocked_items,
                    &mut packet_list,
                ) {
                    // The client isn't interested in key data, it also
                    // would be additional information that would need
                    // cleaning up - so we just send an empty packet.
                    packet_list.push(OutPacket {
                        header: OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
                        body: String::new(),
                        blocker_id: packet_blocker_id,
                    });
                } else {
                    // send plaintext
                    packet_list.push(OutPacket {
                        header: OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
                        body: serde_json::to_string(&plaintext).unwrap(),
                        blocker_id: packet_blocker_id,
                    });
                }
            }

            // If a new session was created, we have to first store
            // all the new information and then try decrypting again.
            Ok(OlmDecryptResult::NewSession(session)) => {
                // First we chache the newly created session.
                session_cache.cache_olm_session(sender_key.clone(), session);

                packet_list.push(OutPacket {
                    header: OutPacketType::Store(StorageDesc::OlmSession {
                        device_id: device_id.to_string(),
                        sender_key: sender_key.to_string(),
                    }),
                    body: session_cache.serialise_sessions_for(&sender_key).unwrap(),
                    blocker_id: None,
                });

                // Due to the creation of a new session, our
                // olm account removed the corresponding OTK.
                // We now have to store this new state of the
                // account.
                packet_list.push(OutPacket {
                    header: OutPacketType::Store(StorageDesc::Account {
                        device_id: device_id.to_string(),
                    }),
                    body: account.pickle(PicklingMode::Unencrypted),
                    blocker_id: None,
                });

                match olm_decrypt(
                    &serde_json::to_string(&event).unwrap(),
                    account,
                    session_cache,
                    user_id,
                ) {
                    Ok(OlmDecryptResult::Plaintext(plaintext)) => {
                        // check for m.room_key event
                        if check_for_room_key_event_and_handle(
                            &plaintext.content,
                            &event.room_id,
                            &event.content.sender_key,
                            &plaintext.keys.ed25519,
                            session_cache,
                            blocker_manager,
                            blocked_items,
                            &mut packet_list,
                        ) {
                            // The client isn't interested in key data, it also
                            // would be additional information that would need
                            // cleaning up - so we just send an empty packet.
                            packet_list.push(OutPacket {
                                header: OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
                                body: String::new(),
                                blocker_id: packet_blocker_id,
                            });
                        } else {
                            packet_list.push(OutPacket {
                                header: OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
                                body: serde_json::to_string(&plaintext).unwrap(),
                                blocker_id: packet_blocker_id,
                            });
                        }
                    }
                    _ => panic!(),
                }
            }
            Err(x) => {
                panic!("{}", x);
            }
        }
    }

    packet_list
}

fn check_for_room_key_event_and_handle(
    plain_content: &str,
    room_id: &str,
    sender_key: &str,
    ed25519: &str,
    session_cache: &mut SessionCache,
    blocker_manager: &mut BlockerManager,
    blocked_items: &mut BTreeMap<u32, Blocker>,
    packet_list: &mut Vec<OutPacket>,
) -> bool {
    if let Ok(room_key_event) = serde_json::from_str(plain_content) as Result<RoomKey, _> {
        let identifier = InboundMegolmSessionIdentifier {
            room_id: room_id.to_string(),
            sender_key: sender_key.to_string(),
            session_id: room_key_event.content.session_id.clone(),
        };

        // Checks if the session is already cached, and if not
        // check our stored data.
        if let Some(packet) = handle_room_key(
            identifier,
            room_key_event.content.session_key.clone(),
            ed25519.to_string(),
            session_cache,
            blocker_manager,
            blocked_items,
        ) {
            // forward the returned Load packet
            packet_list.push(packet);
        }

        true
    } else {
        false
    }
}

pub(crate) fn handle_megolm_event(
    event: &MegolmEncrypted,
    blocker_id: Option<u32>,
    session_cache: &mut SessionCache,
    blocker_manager: &mut BlockerManager,
    blocked_items: &mut BTreeMap<u32, Blocker>,
) -> Vec<OutPacket> {
    let mut return_packets = Vec::new();
    let identifier = InboundMegolmSessionIdentifier {
        room_id: event.room_id.clone(),
        sender_key: event.content.sender_key.clone(),
        session_id: event.content.session_id.clone(),
    };

    if session_cache.inbound_megolm_session_cached(&identifier) {
        let session_with_info = session_cache
            .get_cached_inbound_megolm_session(&identifier)
            .unwrap();

        // FIXME: don't unwrap and work with message_index
        // to prevent replay attacks
        let (plaintext, _message_index) = session_with_info
            .session
            .decrypt(event.content.ciphertext.clone())
            .unwrap();

        // send back the plaintext
        return_packets.push(OutPacket {
            header: OutPacketType::ActionRsp(ActionType::ForwardEncryptedEvent),
            body: serde_json::to_string(&MegolmPlaintextPacketBody::from_megolm_encrypted(
                event, &plaintext,
            ))
            .unwrap(),
            blocker_id,
        });
    } else {
        // If the session isn't cached, we have to try loading it from storage.
        let load_session_bid = blocker_manager.next_blocker_id();
        let blocked_event_body = MegolmEncryptedBody {
            event: event.clone(),
            blocker_id: blocker_id.unwrap(),
        };
        let load_session_blocked_event = Blocker::MegolmDecrypt(blocked_event_body);

        blocked_items.insert(load_session_bid, load_session_blocked_event);

        // send load request to module
        let session_load_req = OutPacket {
            header: OutPacketType::Load(StorageDesc::InboundMegolmSession {
                room_id: identifier.room_id,
                sender_key: identifier.sender_key,
                session_id: identifier.session_id,
            }),
            body: String::new(),
            blocker_id,
        };

        return_packets.push(session_load_req);
    }

    return_packets
}

pub(crate) fn create_new_outbound_megolm_session(
    event_to_encrypt: C2MEncryptEventBody,
    blocker_id: Option<u32>,
    rotation_period_ms: u64,
    rotation_period_msgs: u32,
    blocker_manager: &mut BlockerManager,
    blocked_items: &mut BTreeMap<u32, Blocker>,
    session_cache: &mut SessionCache,
    account: &OlmAccount,
    sender: Sender<OutPacket>,
) {
    let session_new = OlmOutboundGroupSession::new();
    // create new session info
    let session_info_new = OutboundGroupSessionWithInfo {
        session: session_new,
        rotation_period_ms,
        rotation_period_msgs,
        // current time
        created_at: SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs(),
        // no messages have been sent using this session yet
        msgs_sent: 0,
    };

    // store the session for later
    sender
        .send(OutPacket {
            header: OutPacketType::Store(StorageDesc::OutboundMegolmSession {
                room_id: event_to_encrypt.room_id.clone(),
            }),
            body: serde_json::to_string(&OutboundGroupSessionWithInfoForStorage::from_active(
                &session_info_new,
            ))
            .unwrap(),
            blocker_id: None,
        })
        .unwrap();

    let session_key = session_info_new.session.session_key();
    let identity_keys: OlmIdentityKeys = serde_json::from_str(&account.identity_keys()).unwrap();

    // cache the new session in the old session's place
    session_cache.cache_outbound_megolm_session(session_info_new, event_to_encrypt.room_id.clone());

    // create corresponding inbound Megolm session
    {
        // we can unwrap here bacause the key we are supplying is the direct
        // result of us directly calling libolm and therefore always correct
        let inbound_megolm_session = OlmInboundGroupSession::new(&session_key).unwrap();
        let inbound_megolm_identifier = InboundMegolmSessionIdentifier {
            room_id: event_to_encrypt.room_id.clone(),
            sender_key: identity_keys.curve25519,
            session_id: inbound_megolm_session.session_id(),
        };
        // send the inbound Megolm session to be stored
        sender
            .send(OutPacket {
                header: OutPacketType::Store(StorageDesc::InboundMegolmSession {
                    room_id: inbound_megolm_identifier.room_id.clone(),
                    sender_key: inbound_megolm_identifier.sender_key.clone(),
                    session_id: inbound_megolm_identifier.session_id.clone(),
                }),
                body: serde_json::to_string(&InboundGroupSessionWithKeyForStorage {
                    session: inbound_megolm_session.pickle(PicklingMode::Unencrypted),
                    ed25519: identity_keys.ed25519.clone(),
                })
                .unwrap(),
                blocker_id: None,
            })
            .unwrap();

        // send the inbound Megolm session to be stored
        sender
            .send(OutPacket {
                header: OutPacketType::Store(StorageDesc::InboundMegolmSession {
                    room_id: inbound_megolm_identifier.room_id.clone(),
                    sender_key: inbound_megolm_identifier.sender_key.clone(),
                    session_id: inbound_megolm_identifier.session_id.clone(),
                }),
                body: serde_json::to_string(&InboundGroupSessionWithKeyForStorage {
                    session: inbound_megolm_session.pickle(PicklingMode::Unencrypted),
                    ed25519: identity_keys.ed25519.clone(),
                })
                .unwrap(),
                blocker_id: None,
            })
            .unwrap();

        session_cache.cache_inbound_megolm_session(
            inbound_megolm_identifier.clone(),
            inbound_megolm_session,
            identity_keys.ed25519.clone(),
        );
    }

    // Blocker ID for getting the users for this room
    let get_users_list_bid = blocker_manager.next_blocker_id();
    // Blocker ID for getting the device list of the users for this room
    let get_devices_bid = blocker_manager.next_blocker_id();

    // SelfInfo for sending the device request after having received the list
    // of users in the room.
    let blocked_user_list_self_info = Blocker::RequestedUsersForRoom(get_devices_bid);

    // Once we have received the device list, we start the encryption process again,
    // which means we'll need the event information again.
    // We store it here as a string in the body.
    let blocked_device_list_self_info =
        Blocker::RequestedDeviceKeys(event_to_encrypt.clone(), blocker_id.unwrap());

    blocked_items.insert(get_users_list_bid, blocked_user_list_self_info);
    blocked_items.insert(get_devices_bid, blocked_device_list_self_info);

    sender
        .send(OutPacket {
            header: OutPacketType::RequestInfo(ModuleInfoReq::UsersForRoom {
                room_id: event_to_encrypt.room_id.clone(),
            }),
            body: String::new(),
            blocker_id: Some(get_users_list_bid),
        })
        .unwrap();
}

pub(crate) fn try_encrypt_room_message(
    event_to_encrypt: C2MEncryptEventBody,
    blocker_id: Option<u32>,
    blocker_manager: &mut BlockerManager,
    blocked_items: &mut BTreeMap<u32, Blocker>,
    session_cache: &mut SessionCache,
    account: &OlmAccount,
    device_id: String,
    sender: Sender<OutPacket>,
) {
    // check if we have a session for the room the event is supposed to be sent to
    if let Some(ref mut session_info) =
        session_cache.get_mut_cached_outbound_megolm_session(&event_to_encrypt.room_id)
    {
        // if yes, we check if the session needs to be rotated
        if outbound_megolm_session_needs_rotation(&session_info) {
            create_new_outbound_megolm_session(
                event_to_encrypt,
                blocker_id,
                session_info.rotation_period_ms,
                session_info.rotation_period_msgs,
                blocker_manager,
                blocked_items,
                session_cache,
                account,
                sender,
            );
        } else {
            // if not we just encrypt and return
            let room_message = &event_to_encrypt.event;

            // create the payload that we will turn into ciphertext
            let payload = MegolmEncryptedPayload {
                r#type: room_message.r#type.clone(),
                content: serde_json::to_string(&room_message.content).unwrap(),
                room_id: room_message.room_id.clone(),
            };
            let payload_encrypted = session_info
                .session
                .encrypt(serde_json::to_string(&payload).unwrap());
            let identity_keys: OlmIdentityKeys =
                serde_json::from_str(&account.identity_keys()).unwrap();

            // create final m.room.encrypted event
            let room_encrypted_event = RoomEncrypted {
                content: RoomEncryptedContent {
                    algorithm: "m.megolm.v1.aes-sha2".to_string(),
                    ciphertext: serde_json::value::Value::String(payload_encrypted),
                    sender_key: identity_keys.curve25519,
                    device_id: Some(device_id),
                    session_id: Some(session_info.session.session_id()),
                },
                r#type: "m.room.encrypted".to_string(),
                room_id: room_message.room_id.clone(),
            };

            // increase session used count
            session_info.msgs_sent += 1;

            // store session with updated info
            sender
                .send(OutPacket {
                    header: OutPacketType::Store(StorageDesc::OutboundMegolmSession {
                        room_id: event_to_encrypt.room_id.clone(),
                    }),
                    body: serde_json::to_string(
                        &OutboundGroupSessionWithInfoForStorage::from_active(&session_info),
                    )
                    .unwrap(),
                    blocker_id: None,
                })
                .unwrap();

            sender
                .send(OutPacket {
                    header: OutPacketType::ActionRsp(ActionType::EncryptEvent),
                    body: serde_json::to_string(&room_encrypted_event).unwrap(),
                    blocker_id,
                })
                .unwrap();
        }
    } else {
        //   if no, we ask the client to get us a potentially previously stored session
        let load_outbound_megolm_session_bid = blocker_manager.next_blocker_id();

        blocked_items.insert(
            load_outbound_megolm_session_bid,
            Blocker::MegolmEncrypt(event_to_encrypt.clone(), blocker_id.unwrap()),
        );

        sender
            .send(OutPacket {
                header: OutPacketType::Load(StorageDesc::OutboundMegolmSession {
                    room_id: event_to_encrypt.room_id.clone(),
                }),
                body: String::new(),
                blocker_id: Some(load_outbound_megolm_session_bid),
            })
            .unwrap();
    }
}

pub fn outbound_megolm_session_needs_rotation(session: &OutboundGroupSessionWithInfo) -> bool {
    // check if amount of messages for this session have been exhausted
    if session.msgs_sent >= session.rotation_period_msgs {
        true
    } else {
        let current_time = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
            Ok(n) => n.as_secs(),
            Err(_) => unreachable!("SystemTime before UNIX EPOCH!"),
        };
        // calculate the time the session has been exhausted
        let rotation_time = session.created_at + (session.rotation_period_ms / 1000);

        current_time >= rotation_time
    }
}

/// Handler for a received m.room_key event.
///
/// We potentially return a Load packet for determining if
/// a session for the provided identifier is already stored.
pub(crate) fn handle_room_key(
    identifier: InboundMegolmSessionIdentifier,
    session_key: String,
    ed25519: String,
    session_cache: &mut SessionCache,
    blocker_manager: &mut BlockerManager,
    blocked_items: &mut BTreeMap<u32, Blocker>,
) -> Option<OutPacket> {
    if !session_cache.inbound_megolm_session_cached(&identifier) {
        // There is no session with the corresponding identifier in our cache,
        // so we have to look at our stored data for a potentially already
        // existing megolm session with our identifier.

        let load_session_blocker_id = blocker_manager.next_blocker_id();

        // SelfInfo packet for later use, in case it turns out that our
        // identifier maps to a new session.
        blocked_items.insert(
            load_session_blocker_id,
            Blocker::RoomKey(RoomKeyBody {
                identifier: identifier.clone(),
                session_key,
                ed25519,
            }),
        );

        Some(OutPacket {
            header: OutPacketType::Load(StorageDesc::InboundMegolmSession {
                room_id: identifier.room_id,
                sender_key: identifier.sender_key,
                session_id: identifier.session_id,
            }),
            body: String::new(),
            blocker_id: Some(load_session_blocker_id),
        })
    } else {
        // The session mapped to by our identifier already exists.
        // There is nothing to do here.
        None
    }
}

/// Encrypting the Megolm session information
/// and sending it to the specified device.
///
/// Specifically for sending outbound Megolm
/// session keys.
pub fn megolm_key_encrypt_and_send(
    session: &OlmSession,
    olm_payload: &OlmPayload,
    recipient_ed25519: String,
    sender_key: String,
    room_id: String,
    sender: Sender<OutPacket>,
) {
    let encrypt_message_type = session.encrypt_message_type();

    let olm_encrypted = session.encrypt(&serde_json::to_string(olm_payload).unwrap());

    let mut ciphertext_map = serde_json::map::Map::new();
    ciphertext_map.insert(
        recipient_ed25519,
        serde_json::to_string(&CiphertextInfo {
            body: olm_encrypted,
            r#type: match encrypt_message_type {
                OlmMessageType::PreKey => 0,
                OlmMessageType::Message => 1,
            },
        })
        .unwrap()
        .into(),
    );

    let encrypted_event = RoomEncrypted {
        content: RoomEncryptedContent {
            algorithm: "m.olm.v1.curve25519-aes-sha2".to_string(),
            ciphertext: serde_json::value::Value::Object(ciphertext_map),
            sender_key,
            device_id: None,
            session_id: None,
        },
        r#type: "m.room.encrypted".to_string(),
        room_id,
    };

    // send encrypted event
    let mut random_buf: Vec<u8> = vec![0; 32];
    {
        let rng = SystemRandom::new();
        rng.fill(random_buf.as_mut_slice()).unwrap();
    }

    // generate random TxnID
    // FIXME: get TxnIDs from the client, this is just
    // supposed to be a dirty hack for now
    let mut txn_id = "m_".to_string(); // have "m_" as a prefix
    for num in random_buf {
        txn_id.push_str(&format!("{:X?}", num));
    }

    sender
        .send(OutPacket {
            header: OutPacketType::HTTPReq(
                HTTPReqType::Put,
                format!(
                    "/_matrix/client/r0/sendToDevice/m.room.encrypted/{}",
                    &txn_id
                ),
            ),
            body: serde_json::to_string(&encrypted_event).unwrap(),
            blocker_id: None,
        })
        .unwrap();
}
