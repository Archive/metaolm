// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// This is a manager entity for generating
/// blocker IDs that are used for the communication
/// between the Matrix client and Metaolm.
#[derive(Copy, Clone)]
pub struct BlockerManager {
    base_counter: u32,
}

impl BlockerManager {
    /// Creates a new entity of `BlockerManager`.
    /// The selected role influences the kind of IDs that will
    /// be generated, in order to avoid collisions.
    /// Metaolm uses even IDs, while the client uses odd ones.
    ///
    /// # Parameters
    /// * part: the role as a participant in Client<=>Metaolm
    ///   communication
    pub fn new(part: Participant) -> Self {
        let base_counter = if part == Participant::Client { 1 } else { 0 };

        BlockerManager { base_counter }
    }

    /// Returns the next blocker ID.
    pub fn next_blocker_id(&mut self) -> u32 {
        let return_value = self.base_counter;
        self.base_counter += 2;

        return_value
    }
}

/// The two communicating participants when using Metaolm.
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Participant {
    Client,
    Metaolm,
}
