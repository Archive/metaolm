// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::{BTreeMap, HashMap};
use std::sync::mpsc::{Receiver, Sender};
use std::{thread, time};

use olm_rs::{
    account::OlmAccount, inbound_group_session::OlmInboundGroupSession, session::OlmSession,
    utility::OlmUtility, PicklingMode,
};

use serde_json;

use crate::blocker::BlockerManager;
use crate::crypto_routines;
use crate::crypto_routines::OlmHandleType;
use crate::json_objects::{
    device::{DeviceKeysRequest, DeviceQueryResponse, SignedDeviceKeys},
    event::{
        Encryption, MegolmEncrypted, OlmEncrypted, OlmPayload, RoomEncrypted,
        RoomEncryptedForSending, RoomKey, RoomKeyContent,
    },
    info::UsersForRoom,
    misc::{MegolmPlaintextPacketBody, MegolmPlaintextPacketBodyContentErr},
    olm::OlmIdentityKeys,
    otk::{OneTimeKeyCount, OneTimeKeyQuery, OneTimeKeyQueryResponse},
    self_info::{MegolmEncryptBody, MegolmEncryptClaimOTKBody},
    storage::InboundGroupSessionWithKeyForStorage,
    transport::C2MEncryptEventBody,
};
use crate::otk_replenisher::OTKReplenisher;
use crate::packet::*;
use crate::session_cache::{InboundMegolmSessionIdentifier, SessionCache};

/// Monolithically encapsulates all functionallity of the Metaolm module.
pub struct Metaolm {
    pub(crate) device_id: String,
    pub(crate) user_id: String,
    pub(crate) olm_account: OlmAccount,
    pub(crate) blocker_manager: BlockerManager,
    pub(crate) blocked_items: BTreeMap<u32, Blocker>,
    pub(crate) session_cache: SessionCache,
    pub(crate) sender: Sender<OutPacket>,
    pub(crate) receiver: Receiver<InPacket>,
}

impl Metaolm {
    pub(crate) fn send(&self, packet: OutPacket) {
        self.sender.send(packet).unwrap();
    }

    /// Initiates the main loop.
    pub fn loop_start(&mut self) {
        let ten_millis = time::Duration::from_millis(10);

        // replenish goal for one time keys is half the amount that an
        // OlmAccount can hold
        let mut otk_replenisher =
            OTKReplenisher::new(self.olm_account.max_number_of_one_time_keys() / 2);

        // initial check for amount of OTKs
        {
            let otk_blocker_id = self.blocker_manager.next_blocker_id();
            self.send(OutPacket::get_otk_count_req(otk_blocker_id));

            self.blocked_items
                .insert(otk_blocker_id, Blocker::DoOTKReplenish);
        }

        let mut loop_finished = false;

        while !loop_finished {
            // check if an OTK replenish cycle is over
            if otk_replenisher.replenish_due() {
                let otk_blocker_id = self.blocker_manager.next_blocker_id();

                self.send(OutPacket::get_otk_count_req(otk_blocker_id));

                self.blocked_items
                    .insert(otk_blocker_id, Blocker::DoOTKReplenish);
            }

            let iter = self.receiver.try_iter();
            for packet in iter {
                match packet.header {
                    InPacketType::TerminateModule => {
                        loop_finished = true;
                        break;
                    }
                    InPacketType::ActionReq(action) => match action {
                        ActionType::EnableEncryption => self.send(OutPacket {
                            header: OutPacketType::ActionRsp(ActionType::EnableEncryption),
                            body: serde_json::to_string(&Encryption::new()).unwrap(),
                            blocker_id: packet.blocker_id,
                        }),
                        ActionType::EncryptEvent => {
                            let event_to_encrypt_result =
                                serde_json::from_str::<C2MEncryptEventBody>(&packet.body);

                            match event_to_encrypt_result {
                                Ok(event_to_encrypt) => {
                                    // call encryption routine
                                    crypto_routines::try_encrypt_room_message(
                                        event_to_encrypt,
                                        packet.blocker_id,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                        &mut self.session_cache,
                                        &self.olm_account,
                                        self.device_id.clone(),
                                        self.sender.clone(),
                                    );
                                }
                                Err(_) => {
                                    self.send(OutPacket::new_module_error("Malformed input", "The provided event for encryption is not a valid m.room_message."));
                                }
                            }
                        }
                        ActionType::ForwardEncryptedEvent => {
                            let event: RoomEncrypted = serde_json::from_str(&packet.body).unwrap();
                            match event.content.algorithm.as_str() {
                                "m.olm.v1.curve25519-aes-sha2" => {
                                    // Returned packets will either contain plaintext,
                                    // ask if a specific olm session has previously
                                    // been sored, or store a newly created olm session.
                                    let olm_event: OlmEncrypted =
                                        serde_json::from_str(&packet.body).unwrap();

                                    let packets_to_send = crypto_routines::handle_olm_event(
                                        olm_event,
                                        packet.blocker_id,
                                        OlmHandleType::FirstRun,
                                        &self.olm_account,
                                        &mut self.session_cache,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                        &self.user_id,
                                        &self.device_id,
                                    );

                                    for packet in packets_to_send {
                                        self.send(packet);
                                    }
                                }
                                "m.megolm.v1.aes-sha2" => {
                                    let megolm_event: MegolmEncrypted =
                                        serde_json::from_str(&packet.body).unwrap();

                                    let packets_to_send = crypto_routines::handle_megolm_event(
                                        &megolm_event,
                                        packet.blocker_id,
                                        &mut self.session_cache,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                    );

                                    for packet in packets_to_send {
                                        self.send(packet);
                                    }
                                }
                                unsupported_algo => {
                                    self.send(OutPacket::new_module_error(
                                        "Unexpected parameter",
                                        &format!(
                                            "Unknown crypto algorithm provided: {}",
                                            unsupported_algo
                                        ),
                                    ));
                                }
                            }
                        }
                    },
                    _ => {}
                }

                if packet.blocker_id.is_some() {
                    let blocker_id = packet.blocker_id.unwrap();
                    let blocked_item_result = self.blocked_items.remove(&blocker_id);

                    if let Some(blocked_item) = blocked_item_result {
                        // check for SelfInfo packet, if not forward to client
                        match blocked_item {
                            Blocker::DoOTKReplenish => {
                                let otk_blocker_id = self.blocker_manager.next_blocker_id();
                                if let Some(out_packet) = self.do_otk_replenish(
                                    &mut otk_replenisher,
                                    &packet.body,
                                    otk_blocker_id,
                                ) {
                                    self.blocked_items.insert(otk_blocker_id, out_packet);
                                };
                            }
                            Blocker::ClaimOTK(body, _blocker_id) => {
                                let claimed_otk: OneTimeKeyQueryResponse =
                                    serde_json::from_str(&packet.body).unwrap();

                                let mut signed_otk = claimed_otk.extract_otk();

                                let identity_keys: OlmIdentityKeys =
                                    serde_json::from_str(&self.olm_account.identity_keys())
                                        .unwrap();

                                let utility = OlmUtility::new();

                                // check signature of OTK, only continue if valid
                                if utility
                                    .ed25519_verify(
                                        &body.device_keys.keys.ed25519,
                                        &serde_json::to_string(&signed_otk.get_otk_only()).unwrap(),
                                        // FIXME: yeah
                                        signed_otk
                                            .signatures
                                            .iter_mut()
                                            .next()
                                            .unwrap()
                                            .1
                                            .signatures
                                            .iter_mut()
                                            .next()
                                            .unwrap()
                                            .1,
                                    )
                                    .unwrap()
                                {
                                    // create the a new outbound Olm session from the OTK
                                    let outbound_session = OlmSession::create_outbound_session(
                                        &self.olm_account,
                                        &body.device_keys.keys.ed25519,
                                        &signed_otk.key,
                                    )
                                    .unwrap();

                                    // cache the new session
                                    self.session_cache.cache_olm_session(
                                        body.device_keys.keys.curve25519.clone(),
                                        outbound_session,
                                    );

                                    let outbound_session = self
                                        .session_cache
                                        .get_cached_olm_sessions(&body.device_keys.keys.curve25519)
                                        .unwrap()
                                        .iter()
                                        .next()
                                        .unwrap();

                                    // FIXME: store newly created session

                                    // get previously created outbound Megolm session
                                    let outbound_megolm_session = self
                                        .session_cache
                                        .get_cached_outbound_megolm_session(
                                            body.event_to_encrypt.room_id.as_str(),
                                        )
                                        .unwrap();

                                    let room_key_event = RoomKey {
                                        content: RoomKeyContent {
                                            algorithm: "m.megolm.v1.aes-sha2".to_string(),
                                            room_id: body.event_to_encrypt.room_id.clone(),
                                            session_id: outbound_megolm_session
                                                .session
                                                .session_id(),
                                            session_key: outbound_megolm_session
                                                .session
                                                .session_key(),
                                        },
                                        r#type: "m.room_key".to_string(),
                                    };

                                    let mut olm_payload = OlmPayload {
                                        r#type: "m.room_key".to_string(),
                                        content: serde_json::to_string(&room_key_event).unwrap(),
                                        sender: self.user_id.clone(),
                                        recipient: String::new(), // filled out later
                                        recipient_keys: HashMap::new(), // filled out later
                                        keys: HashMap::new(),
                                    };
                                    olm_payload.keys.insert(
                                        "ed25519".to_string(),
                                        identity_keys.ed25519.clone(),
                                    );
                                    olm_payload.recipient = body.device_keys.user_id.clone();
                                    olm_payload.recipient_keys.insert(
                                        "ed25519".to_string(),
                                        body.device_keys.keys.ed25519.clone(),
                                    );

                                    crypto_routines::megolm_key_encrypt_and_send(
                                        outbound_session,
                                        &olm_payload,
                                        body.device_keys.keys.ed25519.clone(),
                                        identity_keys.ed25519,
                                        body.event_to_encrypt.room_id.clone(),
                                        self.sender.clone(),
                                    );
                                }
                            }
                            Blocker::OlmDecrypt(event, blocker_id) => {
                                let mut session_unpickling_failed = false;
                                // If the packet's body isn't empty, we just
                                // got a previously stored OlmSession.
                                if !packet.body.is_empty() {
                                    // Load the session into our cache.
                                    match self.session_cache.cache_pickled_olm_session(
                                        event.sender.clone(),
                                        packet.body.clone(),
                                    ) {
                                        Ok(_) => {}
                                        Err(x) => {
                                            session_unpickling_failed = true;
                                            self.send(OutPacket::new_module_error(
                                                "Deserialisation error",
                                                &x,
                                            ));
                                        }
                                    };
                                }

                                // In case previously unpickling the session
                                // failed, we can't proceed to decrypting the
                                // event, as the session that would be required
                                // for it isn't there.
                                if !session_unpickling_failed {
                                    let packets_to_send = crypto_routines::handle_olm_event(
                                        event,
                                        Some(blocker_id),
                                        OlmHandleType::SecondRun,
                                        &self.olm_account,
                                        &mut self.session_cache,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                        &self.user_id,
                                        &self.device_id,
                                    );

                                    for packet in packets_to_send {
                                        self.send(packet);
                                    }
                                }
                            }
                            Blocker::MegolmDecrypt(megolm_info) => {
                                let event = megolm_info.event;

                                if !packet.body.is_empty() {
                                    // We got a stored session back, now we have to load it into our cache and
                                    // finally decrypt our message.
                                    let session_info: InboundGroupSessionWithKeyForStorage =
                                        serde_json::from_str(&packet.body).unwrap();
                                    let session = OlmInboundGroupSession::unpickle(
                                        session_info.session,
                                        PicklingMode::Unencrypted,
                                    )
                                    .unwrap();

                                    let identifier = InboundMegolmSessionIdentifier {
                                        room_id: event.room_id.clone(),
                                        sender_key: event.content.sender_key.clone(),
                                        session_id: event.content.session_id.clone(),
                                    };

                                    self.session_cache.cache_inbound_megolm_session(
                                        identifier,
                                        session,
                                        session_info.ed25519,
                                    );

                                    let packets_to_send = crypto_routines::handle_megolm_event(
                                        &event,
                                        Some(megolm_info.blocker_id),
                                        &mut self.session_cache,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                    );

                                    for packet in packets_to_send {
                                        self.send(packet);
                                    }
                                } else {
                                    // No session was stored, so we'll notify the
                                    // client accordingly.
                                    self.send(OutPacket {
                                        header: OutPacketType::ActionRsp(
                                            ActionType::ForwardEncryptedEvent,
                                        ),
                                        body: serde_json::to_string(&MegolmPlaintextPacketBody {
                                            content: Err(MegolmPlaintextPacketBodyContentErr {
                                                error: "SESSION_NOT_PRESENT".to_string(),
                                            }),
                                            origin_server_ts: event.origin_server_ts,
                                            room_id: event.room_id.clone(),
                                            r#type: "m.room.message".to_string(),
                                        })
                                        .unwrap(),
                                        blocker_id: Some(megolm_info.blocker_id),
                                    });
                                }
                            }
                            Blocker::OTKListPublished => {
                                self.olm_account.mark_keys_as_published();
                                self.send(OutPacket {
                                    header: OutPacketType::Store(StorageDesc::Account {
                                        device_id: self.device_id.clone(),
                                    }),
                                    body: self.olm_account.pickle(PicklingMode::Unencrypted),
                                    blocker_id: None,
                                });
                            }
                            Blocker::RoomKey(room_key_body) => {
                                // packet is a StorageRsp, so we have to check if there
                                // was a Megolm session previously stored.

                                if packet.body.is_empty() {
                                    // No session with the identifier in question already
                                    // exists, so we have to create a new one.
                                    let megolm_session =
                                        OlmInboundGroupSession::new(&room_key_body.session_key)
                                            .unwrap();

                                    let megolm_session_for_storage =
                                        InboundGroupSessionWithKeyForStorage {
                                            session: megolm_session
                                                .pickle(PicklingMode::Unencrypted),
                                            ed25519: room_key_body.ed25519.clone(),
                                        };

                                    // store newly created Megolm session
                                    self.send(OutPacket {
                                        header: OutPacketType::Store(
                                            StorageDesc::InboundMegolmSession {
                                                room_id: room_key_body.identifier.room_id.clone(),
                                                sender_key: room_key_body
                                                    .identifier
                                                    .sender_key
                                                    .clone(),
                                                session_id: room_key_body
                                                    .identifier
                                                    .session_id
                                                    .clone(),
                                            },
                                        ),
                                        body: serde_json::to_string(&megolm_session_for_storage)
                                            .unwrap(),
                                        blocker_id: None,
                                    });

                                    // cache it
                                    self.session_cache.cache_inbound_megolm_session(
                                        room_key_body.identifier,
                                        megolm_session,
                                        room_key_body.ed25519,
                                    );
                                } else {
                                    // The session already exists, but it is probably
                                    // a good idea to load it into our cache.
                                    let megolm_session = OlmInboundGroupSession::unpickle(
                                        packet.body,
                                        PicklingMode::Unencrypted,
                                    )
                                    .unwrap();
                                    self.session_cache.cache_inbound_megolm_session(
                                        room_key_body.identifier,
                                        megolm_session,
                                        room_key_body.ed25519,
                                    );
                                }
                            }
                            Blocker::MegolmEncrypt(event_to_encrypt, blocker_id) => {
                                // we had a session previously stored
                                if !packet.body.is_empty() {
                                    let stored_session: MegolmEncryptBody =
                                        serde_json::from_str(&packet.body).unwrap();
                                    self.session_cache.cache_outbound_megolm_session(
                                        stored_session.session.into_active(),
                                        stored_session.room_id,
                                    );

                                    crypto_routines::try_encrypt_room_message(
                                        event_to_encrypt,
                                        packet.blocker_id,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                        &mut self.session_cache,
                                        &self.olm_account,
                                        self.device_id.clone(),
                                        self.sender.clone(),
                                    );

                                // no previously stored session, so we have to create a new one
                                } else {
                                    // create new session and add event_to_encrypt to blocked_items
                                    crypto_routines::create_new_outbound_megolm_session(
                                        event_to_encrypt,
                                        Some(blocker_id),
                                        // FIXME: default values for now, find out how to
                                        // get the actual values
                                        7 * 24 * 60 * 60 * 1000, // a week in ms
                                        100,
                                        &mut self.blocker_manager,
                                        &mut self.blocked_items,
                                        &mut self.session_cache,
                                        &self.olm_account,
                                        self.sender.clone(),
                                    );
                                }
                            }
                            Blocker::RequestedUsersForRoom(blocker_id) => {
                                // we have received the list of users for the room
                                let users_list: UsersForRoom =
                                    serde_json::from_str(&packet.body).unwrap();

                                let mut device_keys_req = DeviceKeysRequest::new();
                                device_keys_req.add_users(users_list.users);

                                self.send(OutPacket {
                                    header: OutPacketType::HTTPReq(
                                        HTTPReqType::Post,
                                        "/_matrix/client/r0/keys/query".to_string(),
                                    ),
                                    body: serde_json::to_string(&device_keys_req).unwrap(),
                                    blocker_id: Some(blocker_id),
                                });
                            }
                            Blocker::RequestedDeviceKeys(event_to_encrypt, blocker_id) => {
                                let device_query_rsp: DeviceQueryResponse =
                                    serde_json::from_str(&packet.body).unwrap();
                                // mapping of user IDs to the associated keys
                                let mut clean_devices: Vec<SignedDeviceKeys> = Vec::new();

                                // First we have to check if the inner user_id and device_id are
                                // equal to those in the signed device keys object.
                                // This prevents a malicious homeserver from replacing keys, without
                                // us noticing.
                                for (user_id, device_list) in device_query_rsp.device_keys {
                                    for (device_id, signed_device_keys) in device_list {
                                        // if they don't match we simply ignore and don't further process
                                        if user_id == signed_device_keys.user_id
                                            && device_id == signed_device_keys.device_id
                                        {
                                            clean_devices.push(signed_device_keys);
                                        }
                                    }
                                }

                                // Now we have to check if the signature on each device is correct.

                                // FIXME: impl logic if a device has been seen before or not
                                // device parameters to remember: (user_id, device_id, ed25519)
                                //
                                // ^ check if device has been seen before,
                                // and if information deviates from previously known info
                                {
                                    let utility = OlmUtility::new();

                                    // the closure checks if the signature is valid
                                    clean_devices.retain(|signed_device| {
                                        let sign_key =
                                            signed_device.get_device_keys_only().keys.ed25519;
                                        let device_keys_no_sig = serde_json::to_string(
                                            &signed_device.get_device_keys_only(),
                                        )
                                        .unwrap();

                                        if let Some(mut signature) =
                                            signed_device.get_own_signature()
                                        {
                                            // either the signature is valid, or we abort
                                            // processing for the device
                                            utility
                                                .ed25519_verify(
                                                    sign_key.as_str(),
                                                    &device_keys_no_sig,
                                                    signature.as_mut_str(),
                                                )
                                                .unwrap()
                                        } else {
                                            // no signature is present, the device can't be trusted
                                            false
                                        }
                                    });
                                }

                                let outbound_megolm_session = self
                                    .session_cache
                                    .get_cached_outbound_megolm_session(&event_to_encrypt.room_id)
                                    .unwrap();

                                let room_key_event = RoomKey {
                                    content: RoomKeyContent {
                                        algorithm: "m.megolm.v1.aes-sha2".to_string(),
                                        room_id: event_to_encrypt.room_id.clone(),
                                        session_id: outbound_megolm_session.session.session_id(),
                                        session_key: outbound_megolm_session.session.session_key(),
                                    },
                                    r#type: "m.room_key".to_string(),
                                };

                                let mut olm_payload = OlmPayload {
                                    r#type: "m.room_key".to_string(),
                                    content: serde_json::to_string(&room_key_event).unwrap(),
                                    sender: self.user_id.clone(),
                                    recipient: String::new(), // filled out later
                                    recipient_keys: HashMap::new(), // filled out later
                                    keys: HashMap::new(),
                                };

                                // fill in with own ed25519 key
                                let identity_keys: OlmIdentityKeys =
                                    serde_json::from_str(&self.olm_account.identity_keys())
                                        .unwrap();
                                olm_payload
                                    .keys
                                    .insert("ed25519".to_string(), identity_keys.ed25519.clone());

                                for device_key in clean_devices {
                                    // fill Olm payload with recipient key info
                                    olm_payload.recipient = device_key.user_id.clone();
                                    olm_payload.recipient_keys.insert(
                                        "ed25519".to_string(),
                                        device_key.keys.ed25519.clone(),
                                    );

                                    // check if we have a sesison ready
                                    if let Some(olm_sessions) = self
                                        .session_cache
                                        .get_cached_olm_sessions(&device_key.keys.ed25519)
                                    {
                                        if olm_sessions.len() > 1 {
                                            // get the one with the smallest session_id
                                            let mut order_map: HashMap<String, usize> =
                                                HashMap::new();
                                            let mut order_vec: Vec<String> = Vec::new();

                                            for (i, session) in olm_sessions.iter().enumerate() {
                                                order_map.insert(session.session_id(), i);
                                                order_vec.push(session.session_id());
                                            }

                                            order_vec.sort_unstable();

                                            let selected_session_index =
                                                order_map.get(order_vec.get(0).unwrap()).unwrap();

                                            let session =
                                                olm_sessions.get(*selected_session_index).unwrap();

                                            crypto_routines::megolm_key_encrypt_and_send(
                                                session,
                                                &olm_payload,
                                                device_key.keys.ed25519.clone(),
                                                identity_keys.ed25519.clone(),
                                                event_to_encrypt.room_id.clone(),
                                                self.sender.clone(),
                                            );
                                        } else if let Some(session) = olm_sessions.get(0) {
                                            // encrypt
                                            crypto_routines::megolm_key_encrypt_and_send(
                                                session,
                                                &olm_payload,
                                                device_key.keys.ed25519.clone(),
                                                identity_keys.ed25519.clone(),
                                                event_to_encrypt.room_id.clone(),
                                                self.sender.clone(),
                                            );
                                        } else {
                                            // FIXME: make this an error instead
                                            unreachable!();
                                        }
                                    } else {
                                        // We'll have to create a new sesison here and in order to do that
                                        // we have to claim a one-time-key (OTK) for the device.
                                        let claim_otk_bid = self.blocker_manager.next_blocker_id();

                                        self.send(OutPacket {
                                            header: OutPacketType::HTTPReq(
                                                HTTPReqType::Post,
                                                "/keys/claim".to_string(),
                                            ),
                                            body: serde_json::to_string(&OneTimeKeyQuery::new(
                                                device_key.user_id.clone(),
                                                device_key.device_id.clone(),
                                            ))
                                            .unwrap(),
                                            blocker_id: Some(claim_otk_bid),
                                        });

                                        // set up the according blocked item for the callback
                                        self.blocked_items.insert(
                                            claim_otk_bid,
                                            // attach everything we'll need for sending the
                                            // m.room_key event once a session has been established
                                            // to this device
                                            Blocker::ClaimOTK(
                                                MegolmEncryptClaimOTKBody {
                                                    event_to_encrypt: event_to_encrypt.clone(),
                                                    device_keys: device_key.get_device_keys_only(),
                                                },
                                                blocker_id,
                                            ),
                                        );
                                    }
                                }

                                // finally encrypt the room message and send it back to the client
                                self.send(OutPacket {
                                    header: OutPacketType::ActionRsp(ActionType::EncryptEvent),
                                    body: serde_json::to_string(&RoomEncryptedForSending {
                                        algorithm: "m.megolm.v1.aes-sha2".to_string(),
                                        sender_key: identity_keys.ed25519.clone(),
                                        ciphertext: outbound_megolm_session.session.encrypt(
                                            serde_json::to_string(&event_to_encrypt.event).unwrap(),
                                        ),
                                        session_id: outbound_megolm_session.session.session_id(),
                                        device_id: self.device_id.clone(),
                                    })
                                    .unwrap(),
                                    blocker_id: Some(blocker_id),
                                });
                            }
                        }
                    };
                }
            }

            thread::sleep(ten_millis);
        }
    }

    /// Executes the one-time-key replenish action. Returns
    /// `Some(Blocker::OTKListPublished)` when a replenish action was neccessay
    /// and has been issued.
    ///
    /// # Parameters
    /// * `otk_count_response`: Response body from `/_matrix/client/r0/keys/upload` to know
    ///   amount of uploaded one-time-keys on the server
    fn do_otk_replenish(
        &self,
        otk_replenisher: &mut OTKReplenisher,
        otk_count_response: &str,
        blocker_id: u32,
    ) -> Option<Blocker> {
        let otk_count: OneTimeKeyCount = serde_json::from_str(&otk_count_response).unwrap();
        let otk_count = otk_count.one_time_key_counts.signed_curve25519;

        match otk_replenisher.replenish_for(
            otk_count as usize,
            &self.olm_account,
            &self.device_id,
            &self.user_id,
            blocker_id,
        ) {
            Some(otk_upload_request) => {
                self.send(otk_upload_request);
                Some(Blocker::OTKListPublished)
            }
            None => None,
        }
    }
}
