// Metaolm - a stand-alone module for E2EE in the Matrix protocol.
// Copyright (C) 2018  Johannes Hayeß
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use indexmap::map::IndexMap;
use olm_rs::{
    inbound_group_session::OlmInboundGroupSession, outbound_group_session::OlmOutboundGroupSession,
    session::OlmSession, PicklingMode,
};
use serde_derive::{Deserialize, Serialize};
use serde_json;

static OLM_CACHE_SIZE: usize = 1000;
static INBOUND_MEGOLM_CACHE_SIZE: usize = 100;
static OUTBOUND_MEGOLM_CACHE_SIZE: usize = 100;

/// This module has the responsibility to manage session data
/// that will be needed by Metaolm over the course of its operation.
#[derive(Default)]
pub struct SessionCache {
    cached_olm_sessions: IndexMap<String, Vec<OlmSession>>,
    cached_inbound_megolm_sessions:
        IndexMap<InboundMegolmSessionIdentifier, InboundGroupSessionWithKey>,
    /// our outbound Megolm sessions, indexed by room IDs
    cached_outbound_megolm_sessions: IndexMap<String, OutboundGroupSessionWithInfo>,
}

#[derive(Hash, Eq, PartialEq, Serialize, Deserialize, Clone)]
pub struct InboundMegolmSessionIdentifier {
    pub room_id: String,
    /// curve25519 key of the sender
    pub sender_key: String,
    pub session_id: String,
}

/// An inbound megolm session with the associated ed25519
/// fingerprint key for verifying received messages.
pub struct InboundGroupSessionWithKey {
    pub session: OlmInboundGroupSession,
    pub ed25519: String,
}

/// An outbound megolm session with the associated
/// information regarding its rotation.
pub struct OutboundGroupSessionWithInfo {
    pub session: OlmOutboundGroupSession,
    pub rotation_period_ms: u64,
    pub rotation_period_msgs: u32,
    // time the session was created in UNIX time
    pub created_at: u64,
    pub msgs_sent: u32,
}

impl SessionCache {
    /// Creates a new instance of `SessionCache`.
    pub fn new() -> Self {
        SessionCache {
            cached_olm_sessions: IndexMap::new(),
            cached_inbound_megolm_sessions: IndexMap::new(),
            cached_outbound_megolm_sessions: IndexMap::new(),
        }
    }

    /// Deserialises the provided session and then
    /// stores it for later use.
    pub fn cache_pickled_olm_session(
        &mut self,
        curve25519_key: String,
        pickled_session: String,
    ) -> Result<(), String> {
        let session_result = OlmSession::unpickle(pickled_session, PicklingMode::Unencrypted);
        match session_result {
            Ok(x) => {
                while self.cached_olm_sessions.len() >= OLM_CACHE_SIZE {
                    self.cached_olm_sessions.pop();
                }
                if self.cached_olm_sessions.contains_key(&curve25519_key) {
                    let cached_sessions =
                        self.cached_olm_sessions.get_mut(&curve25519_key).unwrap();
                    cached_sessions.push(x);
                } else {
                    let new_session_list = vec![x];
                    self.cached_olm_sessions
                        .insert(curve25519_key, new_session_list);
                }
                Ok(())
            }
            Err(x) => Err(format!(
                "Error when trying to searilise provided OlmSession: {:?}",
                x
            )),
        }
    }

    /// Store the provided session for later use.
    pub fn cache_olm_session(&mut self, curve25519_key: String, session: OlmSession) {
        while self.cached_olm_sessions.len() >= OLM_CACHE_SIZE {
            self.cached_olm_sessions.pop();
        }
        if self.cached_olm_sessions.contains_key(&curve25519_key) {
            let cached_sessions = self.cached_olm_sessions.get_mut(&curve25519_key).unwrap();
            cached_sessions.push(session);
        } else {
            let new_session_list = vec![session];
            self.cached_olm_sessions
                .insert(curve25519_key, new_session_list);
        }
    }

    /// Serialises the sessions inserted for the supplied
    /// fingerprint key.
    /// Serialised olm sessions can be imported again with
    /// `deserialise_and_insert_sessions_for`.
    pub fn serialise_sessions_for(&self, curve25519_key: &str) -> Option<String> {
        match self.get_cached_olm_sessions(curve25519_key) {
            Some(sessions) => {
                let mut sessions_serialised: Vec<String> = Vec::new();
                for session in sessions {
                    sessions_serialised.push(session.pickle(PicklingMode::Unencrypted));
                }

                Some(serde_json::to_string(&sessions_serialised).unwrap())
            }
            None => None,
        }
    }

    /// Deserialises the supplied sessions parameter and inserts
    /// the session list for the supplied fingerprint key.
    ///
    /// # Parameters
    /// * curve25519_key: fingerprint key to which the sessions belong
    /// * sessions: List of serialised `OlmSession`s
    pub fn deserialise_and_insert_sessions_for(
        &mut self,
        curve25519_key: String,
        sessions: Vec<String>,
    ) {
        let mut sessions_deserialised: Vec<OlmSession> = Vec::new();
        for session in sessions {
            sessions_deserialised
                .push(OlmSession::unpickle(session, PicklingMode::Unencrypted).unwrap());
        }

        self.cached_olm_sessions
            .insert(curve25519_key, sessions_deserialised);
    }

    /// Checks if there are any sessions cached for the provided
    /// fingerprint key.
    pub fn olm_session_cached(&self, curve25519_key: &str) -> bool {
        self.cached_olm_sessions.contains_key(curve25519_key)
    }

    /// Get the sessions associated with the fingerprint key that was
    /// provided.
    pub fn get_cached_olm_sessions(&self, curve25519_key: &str) -> Option<&Vec<OlmSession>> {
        self.cached_olm_sessions.get(curve25519_key)
    }

    /// Caches the supplied group session for later use.
    /// The session identifier has to be constructed beforehand with the
    /// information that uniquely identifies it.
    ///
    /// See: [Matrix E2E impl. guide](https://matrix.org/docs/guides/e2e_implementation.html#handling-an-room-key-event)
    pub fn cache_inbound_megolm_session(
        &mut self,
        session_identifier: InboundMegolmSessionIdentifier,
        session: OlmInboundGroupSession,
        ed25519: String,
    ) {
        // make sure we maintain the correct maximum amount of inbound sessions
        while self.cached_olm_sessions.len() >= INBOUND_MEGOLM_CACHE_SIZE {
            self.cached_inbound_megolm_sessions.pop();
        }

        self.cached_inbound_megolm_sessions.insert(
            session_identifier,
            InboundGroupSessionWithKey { session, ed25519 },
        );
    }

    /// Gets the inbound megolm session identified by the supplied function parameters.
    pub fn get_cached_inbound_megolm_session(
        &self,
        session_identifier: &InboundMegolmSessionIdentifier,
    ) -> Option<&InboundGroupSessionWithKey> {
        self.cached_inbound_megolm_sessions.get(session_identifier)
    }

    pub fn inbound_megolm_session_cached(
        &self,
        session_identifier: &InboundMegolmSessionIdentifier,
    ) -> bool {
        self.cached_inbound_megolm_sessions
            .contains_key(session_identifier)
    }

    pub fn outbound_megolm_session_cached(&self, room_id: &str) -> bool {
        self.cached_outbound_megolm_sessions.contains_key(room_id)
    }

    pub fn get_cached_outbound_megolm_session(
        &self,
        room_id: &str,
    ) -> Option<&OutboundGroupSessionWithInfo> {
        self.cached_outbound_megolm_sessions.get(room_id)
    }

    pub fn get_mut_cached_outbound_megolm_session(
        &mut self,
        room_id: &str,
    ) -> Option<&mut OutboundGroupSessionWithInfo> {
        self.cached_outbound_megolm_sessions.get_mut(room_id)
    }

    pub fn cache_outbound_megolm_session(
        &mut self,
        session_info: OutboundGroupSessionWithInfo,
        room_id: String,
    ) {
        // make sure we maintain the correct maximum amount of inbound sessions
        while self.cached_olm_sessions.len() >= OUTBOUND_MEGOLM_CACHE_SIZE {
            self.cached_outbound_megolm_sessions.pop();
        }

        self.cached_outbound_megolm_sessions
            .insert(room_id, session_info);
    }
}
