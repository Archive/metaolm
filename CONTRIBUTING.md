# Contributing to metaolm 

Thank you for considering to contribute to this project!

If you have something in specific in mind that you would like to
help us with, make sure to announce your intention to do so in
either the #olm-rs discussion room, or directly on the issue tracker 
on [GNOME's Gitlab](https://gitlab.gnome.org/jhaye/metaolm/issues).
This ensures that work won't be unnecessarily duplicated.

In case you are just here to help generally, both of these places
are an equally great place to ask or look for what still is to be
done.

### References
To understand how end-to-end encryption should be implemented on 
the client side, have a look at the 
[E2EE implementation guide for Matrix](https://matrix.org/docs/guides/e2e_implementation.html).

Instead of directly using [libolm](https://git.matrix.org/git/olm/about/)
for cryptographic purposes we are using [olm-rs](https://crates.io/crates/olm-rs),
a safe and Rust idiomatic wrapper around libolm.

### Contributing guidelines

You can directly submit merge requests on 
[GNOME's Gitlab](https://gitlab.gnome.org/jhaye/metaolm) or 
send patches directly to me via email at `jhaye[at]mailbox.org`.

Before submitting a patch, make sure of the following things:

* all unit tests pass
* your code is formatted using `rustfmt`, for consistency

If you added functionality that is currently not covered by unit tests, it would be highly appreciated if you could implemented the according test cases.
